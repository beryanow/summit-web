import React from "react";
import {NavLink} from "react-router-dom";

import {FiTarget, GiKnifeFork, FaLightbulb, FaDumbbell, FaUserAlt} from "react-icons/all";

import classes from "./SectionFooter.module.css";


const SectionFooter = ({createLink}) => {
    return (
        <div className={classes.sectionFooter}>
            <div className={classes.serviceNavigation}>
                <NavLink to="/goals" className={classes.navItem} activeClassName={classes.activeNavItem}>
                    <FiTarget />
                </NavLink>
                <NavLink to="/nutrition" className={classes.navItem} activeClassName={classes.activeNavItem}>
                    <GiKnifeFork />
                </NavLink>
                <NavLink to="/ideas" className={classes.navItem} activeClassName={classes.activeNavItem}>
                    <FaLightbulb />
                </NavLink>
                <NavLink to="/training" className={classes.navItem} activeClassName={classes.activeNavItem}>
                    <FaDumbbell />
                </NavLink>
                <NavLink to="/profile" className={classes.navItem} activeClassName={classes.activeNavItem}>
                    <FaUserAlt />
                </NavLink>
            </div>
            {!!createLink &&
                <NavLink to={createLink} className={classes.createLink} >
                    #создать
                </NavLink>
            }
        </div>
    );
};

export default SectionFooter;