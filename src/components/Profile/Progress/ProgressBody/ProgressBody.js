import React from 'react';
import { AiOutlineDoubleLeft, AiOutlineDoubleRight } from 'react-icons/all';

import WidgetScreens from './WidgetScreens/WidgetScreens';
import classes from './ProgressBody.module.css';


const ProgressBody = ({ progress }) => {
    const goRight = () => {
        const element = document.getElementById("progress-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft + element.clientWidth,
            behavior: "smooth"
        });
    }

    const goLeft = () => {
        const element = document.getElementById("progress-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft - element.clientWidth,
            behavior: "smooth"
        });
    }

    return (
        <div className={ classes.progressBody }>
            <div
                onClick={ goLeft }
                className={ classes.screensControl }
            >
                <AiOutlineDoubleLeft />
            </div>
            <WidgetScreens progress={ progress } />
            <div
                onClick={ goRight }
                className={ classes.screensControl }
            >
                <AiOutlineDoubleRight />
            </div>
        </div>
    );
}

export default ProgressBody;
