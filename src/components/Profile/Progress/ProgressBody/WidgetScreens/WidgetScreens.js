import React from 'react';

import ProgressWidget from './ProgressWidget/ProgressWidget';
import classes from './WidgetScreens.module.css';


const WidgetScreens = ({ progress }) => {
    const progressElements = progress.map(progressElement =>
        <ProgressWidget key={ progressElement.id } progressElement={ progressElement } />
    );

    return (
        <div id="progress-widgets-screens" className={ classes.widgetScreens } >
            {progressElements}
        </div>
    );
}

export default WidgetScreens;
