import React from 'react';
import classes from './ProgressWidget.module.css';


const ProgressWidget = ({progressElement}) => {
    const {image, date} = progressElement;

    const widgetBackgroundStr = image ? `data:image/png;base64,${image}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    return (
        <div style={widgetBackgroundStyle} className={classes.progressWidget}>
            <span className={classes.progressWidgetDate}>{date}</span>
        </div>
    );
}

export default ProgressWidget;
