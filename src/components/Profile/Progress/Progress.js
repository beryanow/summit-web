import React from 'react';
import { FaUserAlt } from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import ProgressBody from './ProgressBody/ProgressBody';
import ProgressButtons from './ProgressButtons/ProgressButtons';

import classes from './Progress.module.css';


const Progress = ({
    progress,
    isSubmitting,
    deleteAllProgress,
    redirectToMainHandle
}) => {
    const headerText = "#профиль";
    const headerIcon = <FaUserAlt />;

    return (
        <div className={ classes.progress }>
            <SectionHeader text={ headerText } icon={ headerIcon } />
            <ProgressBody progress={ progress } />
            <ProgressButtons
                isSubmitting={ isSubmitting }
                deleteAllProgress={ deleteAllProgress }
                redirectToMainHandle={ redirectToMainHandle }
            />
        </div>
    );
}

export default Progress;
