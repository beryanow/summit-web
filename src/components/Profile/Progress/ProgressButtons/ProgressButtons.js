import React from 'react';
import classes from './ProgressButtons.module.css';


const ProgressButtons = ({
    isSubmitting,
    deleteAllProgress,
    redirectToMainHandle
}) => (
    <div className={ classes.progressButtons }>
        <button
            type="button"
            onClick={ deleteAllProgress }
            disabled={ isSubmitting }
            className={ `${classes.progressButton} ${classes.deleteButton}` }
        >
            Удалить весь прогресс
        </button>
        <button
            type="button"
            onClick={ redirectToMainHandle }
            disabled={ isSubmitting }
            className={ classes.progressButton }
        >
            Назад
        </button>
    </div>
);

export default ProgressButtons;
