import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import Progress from './Progress';


class ProgressContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            isDeleted: false,
            redirectToMain: false
        };
    }

    componentDidMount() {
        const {backgroundsInfo, authInfo} = this.props;
        const {backgrounds} = backgroundsInfo;
        const {token} = authInfo;

        this.props.getProgress(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { profileInfo } = this.props;
        const { dataDeleted, error } = profileInfo;

        if (dataDeleted) this.deleteSuccessHandle();
        else if (error) this.operationErrorHandle(error);
    }

    deleteSuccessHandle = () => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false, isDeleted: true });

        toast.success("Весь прогресс удален успешно!");
    }

    operationErrorHandle = error => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false });

        toast.error(error);
    }

    redirectToMainHandle = () => this.setState({ redirectToMain: true });

    deleteAllProgress = () => {
        const {authInfo} = this.props;
        const {token} = authInfo;

        this.props.deleteAllProgress(token);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            isSubmitting,
            isDeleted,
            redirectToMain
        } = this.state;
        if (isDeleted || redirectToMain) return <Redirect push to="/profile" />;

        const { profileInfo } = this.props;
        const { progress } = profileInfo;

        progress.sort((first, second) => {
            const firstDate = new Date(first.date);
            const secondDate = new Date(second.date);

            if (firstDate < secondDate) return -1;
            else if (firstDate > secondDate) return 1;
            else return 0;
        });

        return (
            <Progress
                progress={ progress }
                isSubmitting={ isSubmitting }
                deleteAllProgress={ this.deleteAllProgress }
                redirectToMainHandle={ this.redirectToMainHandle }
            />
        );
    }
}

export default ProgressContainer;
