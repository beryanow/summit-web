import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import CreateProgress from './CreateProgress';


class CreateProgressContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            calendarValue: new Date(),
            imageSuccess: false,
            imageError: false,
            imageErrorMessage: "",
            imageBase64: "",
            redirectToMain: false,
            isSubmitting: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { profileInfo } = this.props;
        const { dataChanged, error } = profileInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.createErrorHandle(error);
    }

    createSuccessHandle = () => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false });

        toast.success("Прогресс успешно создан!");
    }

    createErrorHandle = (error) => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false });

        toast.error(error);
    }


    calendarChange = (v) => {
        this.setState({ calendarValue: v });
    }

    redirectToMainHandle = () => this.setState({ redirectToMain: true });

    imageLoad = ({ target: { files } }) => {
        const validateFile = file => {
            if (!file) return false;

            const MB_1 = 1048576;
            if (file.size > MB_1) {
                this.setState({
                    imageSuccess: false,
                    imageError: true,
                    imageErrorMessage: "Размер файла слишком большой"
                });
                return false;
            }

            if (file.type !== "image/jpeg"
                && file.type !== "image/jpg"
                && file.type !== "image/png") {
                this.setState({
                    imageSuccess: false,
                    imageError: true,
                    imageErrorMessage: "Неподдерживаемый формат файла"
                });
                return false;
            }

            return true;
        }

        const onloadendFunc = () => {
            const backgroundBase64 = reader.result.replace(/^data:image.+;base64,/, '');
            this.setState({
                imageSuccess: true,
                imageError: false,
                imageErrorMessage: "",
                imageBase64: backgroundBase64
            });
        }

        const file = files[0];
        if ( !validateFile(file) ) return;

        const reader = new FileReader();
        reader.addEventListener("loadend", onloadendFunc);
        reader.readAsDataURL(file);
    }

    onCreate = () => {
        const { calendarValue, imageBase64 } = this.state;

        if (!imageBase64) {
            this.setState({
                imageSuccess: false,
                imageError: true,
                imageErrorMessage: "Изображение не загружено"
            });
            return;
        }

        const year = calendarValue.getFullYear();
        const month = calendarValue.getMonth() + 1;
        const day = calendarValue.getDate();

        const { authInfo } = this.props;
        const { userId, token } = authInfo;

        const createdProgress = {
            date: `${year}-${month}-${day}`,
            imageId: 1,
            image: imageBase64,
            profileId: userId
        };

        this.props.createProgress(createdProgress, token);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            calendarValue,
            imageSuccess,
            imageError,
            imageErrorMessage,
            redirectToMain,
            isSubmitting
        } = this.state;
        if (redirectToMain) return <Redirect push to="/profile" />;

        return (
            <CreateProgress
                calendarValue={calendarValue}
                calendarChange={this.calendarChange}
                imageSuccess={imageSuccess}
                imageError={imageError}
                imageErrorMessage={imageErrorMessage}
                imageLoad={this.imageLoad}
                redirectToMainHandle={this.redirectToMainHandle}
                isSubmitting={isSubmitting}
                onCreate={this.onCreate}
            />
        );
    }
}

export default CreateProgressContainer;
