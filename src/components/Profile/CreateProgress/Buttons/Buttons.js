import React from 'react';
import classes from './Buttons.module.css';


const Buttons = ({
    isSubmitting,
    onCreate,
    redirectToMainHandle
}) => (
    <div className={classes.buttons}>
        <button
            type="button"
            onClick={onCreate}
            disabled={isSubmitting}
            className={classes.qButton}
        >
            Создать прогресс
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.qButton}
        >
            Назад
        </button>
    </div>
);

export default Buttons;
