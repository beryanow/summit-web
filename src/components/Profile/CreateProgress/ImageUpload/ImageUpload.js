import React from 'react';
import classes from './ImageUpload.module.css';


const ImageUpload = ({
    isSuccess,
    isError,
    errorMessage,
    onLoad
}) => {
    const additionalClassname = isSuccess ?
        classes.imageUploadStatusSuccess :
        isError ? classes.imageUploadStatusError : "";
    const statusMessage = isSuccess ? "Изображение успешно загружено" : errorMessage;

    return (
        <div className={classes.imageUpload}>
            <label
                htmlFor="progress-upload"
                className={classes.imageUploadLabel}
            >
                Загрузить изображение
            </label>
            <input
                id="progress-upload"
                type="file"
                onChange={onLoad}
                className={classes.imageUploadInput}
            />
            <div className={`${classes.imageUploadStatus} ${additionalClassname}`}>
                {statusMessage}
            </div>
        </div>
    );
}

export default ImageUpload;
