import React from 'react';
import Calendar from 'react-calendar';

import ImageUpload from './ImageUpload/ImageUpload';
import Buttons from './Buttons/Buttons';

import classes from './CreateProgress.module.css';


const CreateProgress = ({
    calendarValue,
    calendarChange,
    imageSuccess,
    imageError,
    imageErrorMessage,
    imageLoad,
    onCreate,
    redirectToMainHandle,
    isSubmitting
}) => {
    return (
        <div className={classes.createProgressWrapper}>
            <div className={classes.createProgress}>
                <span className={classes.createProgressTitle}>Создание прогресса</span>
                <div>
                    <span className={classes.dateProgressTitle}>Дата прогресса</span>
                    <Calendar
                        onChange={calendarChange}
                        value={calendarValue}
                        className={classes.calendar}
                    />
                </div>
                <ImageUpload
                    isSuccess={imageSuccess}
                    isError={imageError}
                    errorMessage={imageErrorMessage}
                    onLoad={imageLoad}
                />
                <Buttons
                    onCreate={onCreate}
                    redirectToMainHandle={redirectToMainHandle}
                    isSubmitting={isSubmitting}
                />
            </div>
        </div>
    );
}

export default CreateProgress;
