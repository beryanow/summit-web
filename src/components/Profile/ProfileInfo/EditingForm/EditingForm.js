import React from 'react';
import { Formik, Form } from 'formik';

import EditingField from './EditingField/EditingField';
import EditingButtons from './EditingButtons/EditingButtons';
import EditingImageUploadContainer from './EditingImageUpload/EditingImageUploadContainer';

import classes from './EditingForm.module.css';


const validate = (values) => {
    const errors = {};
    const {name, surname, age} = values;

    if (!name) errors.name = "Не заполнено";
    if (!surname) errors.surname = "Не заполнено";
    if (!age) errors.age = "Не заполнено";

    return errors;
}

const EditingForm = ({
    isSubmitting,
    setBackgroundBase64,
    initialValues,
    onUpdateSubmit,
    cancelHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={onUpdateSubmit}
    >
        {() => (
            <Form className={classes.editingForm}>
                <span className={classes.editingFormTitle}>Обновление Профиля</span>
                <EditingField id="profile-update-name" name="name" label="Имя" />
                <EditingField id="profile-update-surname" name="surname" label="Фамилия" />
                <EditingField id="profile-update-age" name="age" label="Возраст" />
                <EditingImageUploadContainer setBackgroundBase64={setBackgroundBase64} />
                <EditingButtons
                    isSubmitting={isSubmitting}
                    cancelHandle={cancelHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default EditingForm;
