import React from 'react';

import InfoField from './InfoField/InfoField';
import InfoButtons from './InfoButtons/InfoButtons';

import classes from './InfoForm.module.css';


const InfoForm = ({
    values,
    editingHandle,
    redirectToMainHandle
}) => {
    const { name, surname, age } = values;

    return (
        <div className={classes.infoForm}>
            <span className={classes.infoFormTitle}>Профиль</span>
            <InfoField fieldLabel="Имя" fieldValue={name} />
            <InfoField fieldLabel="Фамилия" fieldValue={surname} />
            <InfoField fieldLabel="Возраст" fieldValue={age} />
            <InfoButtons
                editingHandle={editingHandle}
                redirectToMainHandle={redirectToMainHandle}
            />
        </div>
    );
};

export default InfoForm;
