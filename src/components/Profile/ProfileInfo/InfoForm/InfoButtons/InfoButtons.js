import React from 'react';
import classes from './InfoButtons.module.css';


const InfoButtons = ({
    editingHandle,
    redirectToMainHandle
}) => (
    <div className={classes.infoButtons}>
        <button
            type="button"
            onClick={editingHandle}
            className={classes.infoButton}
        >
            Изменить
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            className={classes.infoButton}
        >
            Назад
        </button>
    </div>
);

export default InfoButtons;