import React from 'react';
import classes from './InfoField.module.css';


const InfoField = ({fieldLabel, fieldValue}) => (
    <div className={classes.infoField}>
        <span className={classes.infoFieldLabel}>{fieldLabel}</span>
        <span className={classes.infoFieldValue}>{fieldValue}</span>
    </div>
);

export default InfoField;
