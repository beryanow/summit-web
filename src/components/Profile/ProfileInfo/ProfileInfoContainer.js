import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import ProfileInfo from './ProfileInfo';
import InfoForm from './InfoForm/InfoForm';
import EditingForm from './EditingForm/EditingForm';


class ProfileInfoContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            isEditing: false,
            deleted: false,
            redirectToMain: false
        };
    }

    componentDidMount() {
        const {profileInfo, backgroundsInfo, authInfo} = this.props;
        const {profile} = profileInfo;

        if (Object.keys(profile).length === 0) {
            const {backgrounds} = backgroundsInfo;
            const {token} = authInfo;

            this.props.getProfile(backgrounds, token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {profileInfo} = this.props;
        const {dataChanged, error} = profileInfo;

        if (dataChanged) this.updateSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    updateSuccessHandle = () => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false, isEditing: false });

        toast.success("Профиль успешно обновлен!");
    }

    operationErrorHandle = () => {
        this.props.profileChangeHandle();
        this.setState({ isSubmitting: false });

        toast.error("Ошибка при выполнении операции!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({ backgroundBase64: backgroundBase64 });
    }

    editingHandle = () => {
        const {isEditing} = this.state;
        this.setState({ backgroundBase64: "", isEditing: !isEditing });
    }

    redirectToMainHandle = () => {
        this.setState({ redirectToMain: true });
    }

    onUpdateSubmit = values => {
        const {name, surname, age} = values;
        const {backgroundBase64} = this.state;

        const {profileInfo: { profile }, authInfo} = this.props;
        const {id, email, imageId, settingsDto} = profile;
        const {userId, token} = authInfo;

        const updatedProfile = {
            id,
            email,
            name,
            surname,
            age,
            imageId,
            background: backgroundBase64,
            settingsDto,
            userId
        };

        this.props.updateProfile(updatedProfile, token);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            isSubmitting,
            isEditing,
            deleted,
            redirectToMain
        } = this.state;
        if (deleted || redirectToMain) return <Redirect push to="/profile" />;

        const {profileInfo: { profile }} = this.props;
        const {name, surname, age} = profile;

        const editingProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            initialValues: {name, surname, age},
            onUpdateSubmit: this.onUpdateSubmit,
            cancelHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const infoProps = {
            values: {name, surname, age},
            editingHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };

        const innerComponent = isEditing ? <EditingForm {...editingProps} /> : <InfoForm {...infoProps} />;
        return <ProfileInfo innerComponent={innerComponent} />;
    }
}

export default ProfileInfoContainer;
