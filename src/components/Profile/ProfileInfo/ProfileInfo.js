import React from 'react';
import classes from './ProfileInfo.module.css';


const ProfileInfo = ({innerComponent}) => (
    <div className={classes.profileInfo}>
        {innerComponent}
    </div>
);

export default ProfileInfo;
