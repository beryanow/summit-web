import React from 'react';
import { Switch, Route } from 'react-router-dom';

import ProfileMainContainer from './ProfileMain/ProfileMainContainer';
import ProfileInfoContainer from './ProfileInfo/ProfileInfoContainer';
import CreateProgressContainer from './CreateProgress/CreateProgressContainer';
import ProgressContainer from './Progress/ProgressContainer';


const Profile = (props) => {
    const {profileInfo, backgroundsInfo, authInfo} = props;
    const {getProfile, updateProfile, createProgress, getProgress, deleteAllProgress, profileChangeHandle} = props;

    const profileMainProps = {profileInfo, backgroundsInfo, authInfo, getProfile, getProgress, profileChangeHandle};
    const profileInfoProps = {profileInfo, backgroundsInfo, authInfo, getProfile, updateProfile, profileChangeHandle};
    const createProgressProps = {profileInfo, backgroundsInfo, authInfo, createProgress, profileChangeHandle};
    const progressProps = {profileInfo, backgroundsInfo, authInfo, getProgress, deleteAllProgress, profileChangeHandle};

    return (
        <Switch>
            <Route exact path="/profile" render={() => <ProfileMainContainer {...profileMainProps} />} />
            <Route exact path="/profile/info" render={() => <ProfileInfoContainer {...profileInfoProps} />} />
            <Route exact path="/profile/add-photo" render={() => <CreateProgressContainer {...createProgressProps} />} />
            <Route exact path="/profile/progress" render={() => <ProgressContainer {...progressProps} />} />
        </Switch>
    );
}

export default Profile;
