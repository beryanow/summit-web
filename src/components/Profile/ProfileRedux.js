import { connect } from 'react-redux';

import Profile from './Profile';
import {
    getProfileFetch,
    updateProfileFetch,
    createProgressFetch,
    getProgressFetch,
    deleteAllProgressFetch,
    profileChangeHandle
} from '../../actions/profileActionCreator';


const mapStateToProps = ({profileInfo, backgroundsInfo, authInfo}) => ({
    profileInfo,
    backgroundsInfo,
    authInfo
})

const mapDispatchToProps = dispatch => ({
    getProfile: (backgrounds, token) => dispatch(getProfileFetch(backgrounds, token)),
    updateProfile: (profile, token) => dispatch(updateProfileFetch(profile, token)),
    createProgress: (progress, token) => dispatch(createProgressFetch(progress, token)),
    getProgress: (backgrounds, token) => dispatch(getProgressFetch(backgrounds, token)),
    deleteAllProgress: (token) => dispatch(deleteAllProgressFetch(token)),
    profileChangeHandle: () => dispatch(profileChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Profile);
