import React from 'react';
import { toast } from 'react-toastify';

import ProfileMain from './ProfileMain';


class ProfileMainContainer extends React.Component {
    componentDidMount() {
        const {backgroundsInfo, authInfo} = this.props;
        const {backgrounds} = backgroundsInfo;
        const {token} = authInfo;

        this.props.getProfile(backgrounds, token);
        this.props.getProgress(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {profileInfo} = this.props;
        const {error} = profileInfo;

        if (error) this.loadErrorHandle();
    }

    loadErrorHandle = (error) => {
        this.props.profileChangeHandle();
        toast.error(error);
    }

    render() {
        const {profileInfo} = this.props;
        const {profile, progress} = profileInfo;

        progress.sort((first, second) => {
            const firstDate = new Date(first.date);
            const secondDate = new Date(second.date);

            if (firstDate < secondDate) return -1;
            else if (firstDate > secondDate) return 1;
            else return 0;
        });

        return <ProfileMain profile={profile} progress={progress} />;
    }
}

export default ProfileMainContainer;
