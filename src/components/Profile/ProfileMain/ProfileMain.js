import React from 'react';
import { FaUserAlt } from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import ProfileBody from './ProfileBody/ProfileBody';

import classes from './ProfileMain.module.css';


const ProfileMain = ({profile, progress}) => {
    const headerText = "#профиль";
    const headerIcon = <FaUserAlt />;

    return (
        <div className={classes.profile}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <ProfileBody profile={profile} progress={progress} />
            <SectionFooter />
        </div>
    );
}

export default ProfileMain;
