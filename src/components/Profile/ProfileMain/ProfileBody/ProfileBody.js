import React from 'react';
import { Link } from 'react-router-dom';

import classes from './ProfileBody.module.css';


const ProfileBody = ({profile, progress}) => {
    const {name, surname, age, email, background} = profile;

    const widgetBackgroundStr = background ? `data:image/png;base64,${background}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    console.log(progress);
    const progressWidgetBackground = progress.length > 0 ? progress[0].image : "";
    const progressWidgetBackgroundStyle = progressWidgetBackground ?
        {backgroundImage: `url(data:image/png;base64,${progressWidgetBackground})`} :
        {};

    return (
        <div className={classes.profileBody}>
            <Link
                to="/profile/info"
                style={widgetBackgroundStyle}
                className={`${classes.profileBodyWidget} ${classes.bigWidget}`}
            >
                <span className={classes.profileBodyWidgetTitle}>Информация</span>
                <div className={classes.profileBodyWidgetContent}>
                    <span className={classes.profileProperties}>Имя: {name} {surname}</span>
                    <span className={classes.profileProperties}>Возраст: {age}</span>
                    <span className={classes.profileProperties}>Email: {email}</span>
                </div>
            </Link>
            <Link
                to="/profile/progress"
                className={`${classes.profileBodyWidget} ${classes.middleWidget}`}
                style={progressWidgetBackgroundStyle}
            >
                <span className={classes.profileBodyWidgetTitle}>Прогресс</span>
                <div className={classes.profileBodyWidgetContent}>
                    <span className={classes.profileProperties}>
                        Оцените ваш силовой прогресс
                    </span>
                </div>
            </Link>
            <Link
                to="/profile/add-photo"
                className={`${classes.profileBodyWidget} ${classes.middleWidget}`}
            >
                <span className={classes.profileBodyWidgetTitle}>Добавить фото</span>
                <div className={classes.profileBodyWidgetContent}>
                    <span className={classes.profileProperties}>
                        Прикрепите фото вашей физической формы
                    </span>
                </div>
            </Link>
        </div>
    );
}

export default ProfileBody;
