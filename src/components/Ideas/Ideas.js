import React from 'react';
import { Switch, Route } from 'react-router-dom';

import IdeasMainContainer from './IdeasMain/IdeasMainContainer';
import IdeasCreateContainer from './IdeasCreate/IdeasCreateContainer';
import DefiniteIdeaContainer from './DefiniteIdea/DefiniteIdeaContainer';


const Ideas = (props) => {
    const searchIdeaById = (id) => {
        for (const idea of ideasInfo.ideas) {
            if (idea.id === Number.parseInt(id)) return idea;
        }

        const placeholderIdea = {name: "", content: ""};
        return placeholderIdea;
    }

    const {ideasInfo, backgroundsInfo, authInfo} = props;
    const {getIdeas, createIdea, updateIdea, deleteIdea, ideaChangeHandle} = props;

    const ideasMainProps = {ideasInfo, backgroundsInfo, authInfo, getIdeas, ideaChangeHandle};
    const ideasCreateProps = {ideasInfo, authInfo, createIdea, ideaChangeHandle};
    const definiteIdeaProps = {ideasInfo, backgroundsInfo, authInfo, getIdeas, updateIdea, deleteIdea, ideaChangeHandle};

    return (
        <Switch>
            <Route exact path="/ideas"  render={() => <IdeasMainContainer {...ideasMainProps} />} />
            <Route exact path="/ideas/create" render={() => <IdeasCreateContainer {...ideasCreateProps} />} />
            <Route exact path="/ideas/:id" render={(props) => <DefiniteIdeaContainer {...definiteIdeaProps} idea={searchIdeaById(props.match.params.id)} />} />
        </Switch>
    );
}

export default Ideas;