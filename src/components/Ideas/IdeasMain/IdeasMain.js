import React from 'react';
import { FaLightbulb } from "react-icons/all";

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import IdeasBody from './IdeasBody/IdeasBody';

import classes from './IdeasMain.module.css';


const IdeasMain = (props) => {
    const {ideas} = props;

    const headerText = "#мысли";
    const headerIcon = <FaLightbulb />;
    const footerLink = "/ideas/create";

    return (
        <div className={classes.ideas}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <IdeasBody ideas={ideas} />
            <SectionFooter createLink={footerLink} />
        </div>
    );
}

export default IdeasMain;