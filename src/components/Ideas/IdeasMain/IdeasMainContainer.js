import React from 'react';
import { toast } from 'react-toastify';

import IdeasMain from './IdeasMain';


class IdeasMainContainer extends React.Component {
    componentDidMount() {
        const {backgroundsInfo, authInfo} = this.props;
        const {backgrounds} = backgroundsInfo;
        const {token} = authInfo;

        this.props.getIdeas(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {ideasInfo} = this.props;
        const {error} = ideasInfo;

        if (error) this.loadErrorHandle();
    }

    loadErrorHandle = () => {
        this.props.ideaChangeHandle();
        toast.error("Не удалось загрузить данные!");
    }

    render() {
        const {ideasInfo} = this.props;
        const {ideas} = ideasInfo;

        ideas.sort((first, second) => {
            const firstIdeaSize = first.size;
            const secondIdeaSize = second.size;

            if (firstIdeaSize > secondIdeaSize) return -1;
            else if (secondIdeaSize > firstIdeaSize) return 1;
            else return 0;
        });

        return (
          <IdeasMain ideas={ideas} />
        );
    }
}

export default IdeasMainContainer;