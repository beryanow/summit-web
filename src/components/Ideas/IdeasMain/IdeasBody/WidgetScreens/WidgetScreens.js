import React from "react";
import classes from "./WidgetScreeens.module.css";


const WidgetScreens = (props) => {
    const {widgetScreens} = props;

    return (
        <div id="ideas-widgets-screens" className={classes.widgetScreens}>
            {widgetScreens}
        </div>
    );
}

export default WidgetScreens;