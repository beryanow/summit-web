import React from 'react';

import WidgetScreens from './WidgetScreens';
import DefiniteScreen from './DefiniteScreen/DefiniteScreen';


class WidgetScreensContainer extends React.Component {
    createWidgetScreens = (ideas) => {
        const widgetScreensObj = {
            widgetScreensArr: [],
            currentIdeaIdx: 0
        };

        while (ideas.length > widgetScreensObj.currentIdeaIdx) {
            this.createNewDefiniteScreen(widgetScreensObj, ideas);
        }

        return widgetScreensObj.widgetScreensArr;
    }

    createNewDefiniteScreen = (widgetScreensObj, ideas) => {
        const screenObj = {
            screenIdeas: [],
            freeCells: 16
        };

        while (screenObj.freeCells > 0 && ideas.length > widgetScreensObj.currentIdeaIdx) {
            const idea = ideas[widgetScreensObj.currentIdeaIdx];
            screenObj.freeCells -= idea.size === "2x2" ? 4 : 2;

            screenObj.screenIdeas.push(idea);

            widgetScreensObj.currentIdeaIdx += 1;
        }

        const definiteScreen = <DefiniteScreen key={widgetScreensObj.currentIdeaIdx} screenIdeas={screenObj.screenIdeas} />;
        widgetScreensObj.widgetScreensArr.push(definiteScreen);
    }


    render() {
        const {ideas} = this.props;
        const widgetScreensArr = this.createWidgetScreens(ideas);

        return (
            <WidgetScreens widgetScreens={widgetScreensArr} />
        );
    }
}

export default WidgetScreensContainer;