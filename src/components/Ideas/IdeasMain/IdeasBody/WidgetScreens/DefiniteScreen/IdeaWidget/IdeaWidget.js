import React from 'react';
import { Link } from 'react-router-dom';

import classes from './IdeaWidget.module.css';


const IdeaWidget = (props) => {
    const {idea} = props;

    const widgetBackgroundStr = idea.background ? `data:image/png;base64,${idea.background}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = idea.size === "2x2" ? classes.bigWidget : classes.middleWidget;

    const widgetLink = `/ideas/${idea.id}`;

    return (
        <Link to={widgetLink} style={widgetBackgroundStyle} className={`${classes.ideaWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.ideaWidgetTitle}>{idea.name}</span>
            <div className={classes.ideaWidgetContent}>
                {idea.content}
            </div>
        </Link>
    );
}

export default IdeaWidget;