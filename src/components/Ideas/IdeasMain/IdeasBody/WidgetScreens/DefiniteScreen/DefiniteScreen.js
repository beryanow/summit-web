import React from 'react';

import IdeaWidget from './IdeaWidget/IdeaWidget';
import classes from './DefiniteScreen.module.css';


const DefiniteScreen = (props) => {
    const {screenIdeas} = props;
    const widgets = screenIdeas.map(idea => <IdeaWidget key={idea.id} idea={idea} />);

    return (
        <div className={classes.widgetScreen}>
            {widgets}
        </div>
    );
}

export default DefiniteScreen;