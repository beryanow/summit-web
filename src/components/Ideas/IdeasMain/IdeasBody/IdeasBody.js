import React from 'react';
import {AiOutlineDoubleLeft, AiOutlineDoubleRight} from 'react-icons/all';

import WidgetScreensContainer from './WidgetScreens/WidgetScreensContainer';
import classes from './IdeasBody.module.css';


const IdeasBody = (props) => {
    const goLeft = () => {
        const element = document.getElementById("ideas-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft - element.clientWidth,
            behavior: "smooth"
        });
    }

    const goRight = () => {
        const element = document.getElementById("ideas-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft + element.clientWidth,
            behavior: "smooth"
        });
    }

    const isMoreOnePageCalc = (ideas) => {
        let summarySize = 0;

        for (const idea of ideas) {
            summarySize += idea.size === "2x2" ? 4 : 2;
            if (summarySize > 16) return true;
        }

        return false;
    }


    const {ideas} = props;
    const isMoreOnePage = isMoreOnePageCalc(ideas);

    return (
        <div className={classes.ideasBody}>
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goLeft()}>
                    <AiOutlineDoubleLeft/>
                </div>
            }
            <WidgetScreensContainer ideas={ideas} />
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goRight()}>
                    <AiOutlineDoubleRight/>
                </div>
            }
        </div>
    );
}

export default IdeasBody;