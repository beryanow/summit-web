import React from 'react';
import classes from './IdeasCreateImageUpload.module.css';


const IdeasCreateImageUpload = ({
    isSuccess,
    isError,
    errorMessage,
    onLoad
}) => {
    const additionalClassname = isSuccess ?
        classes.ideasCreateImageUploadStatusSuccess :
        isError ? classes.ideasCreateImageUploadStatusError : "";
    const statusMessage = isSuccess ? "Изображение успешно загружено" : errorMessage;

    return (
        <div className={classes.ideasCreateImageUpload}>
            <label
                htmlFor="ideas-create-upload"
                className={classes.ideasCreateImageUploadLabel}
            >
                Загрузить изображение
            </label>
            <input
                id="ideas-create-upload"
                type="file"
                onChange={onLoad}
                className={classes.ideasCreateImageUploadInput}
            />
            <div className={`${classes.ideasCreateImageUploadStatus} ${additionalClassname}`}>
                {statusMessage}
            </div>
        </div>
    );
}

export default IdeasCreateImageUpload;