import React from 'react';
import classes from './IdeasCreateButtons.module.css';

const IdeasCreateButtons = ({isSubmitting, redirectToMainHandle}) => (
    <div className={classes.ideasCreateButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.ideasCreateButton}
        >
            Создать идею
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.ideasCreateButton}
        >
            Назад
        </button>
    </div>
);

export default IdeasCreateButtons;