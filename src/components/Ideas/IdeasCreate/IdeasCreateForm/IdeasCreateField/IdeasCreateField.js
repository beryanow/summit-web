import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './IdeasCreateField.module.css';


const IdeasCreateField = ({ id, name, label }) => (
    <div className={classes.ideasCreateField}>
        <label
            htmlFor={id}
            className={classes.ideasCreateFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.ideasCreateFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.ideasCreateFieldError}
        />
    </div>
);

export default IdeasCreateField;