import React from 'react';
import { Formik, Form } from 'formik';

import IdeasCreateField from './IdeasCreateField/IdeasCreateField';
import IdeasCreateButtons from './IdeasCreateButtons/IdeasCreateButtons';
import IdeasCreateImageUploadContainer from './IdeasCreateImageUpload/IdeasCreateImageUploadContainer';

import classes from './IdeasCreateForm.module.css';


const validate = (values) => {
    const errors = {};
    const {name, content} = values;

    if (!name) errors.name = "Не заполнено";
    if (!content) errors.content = "Не заполнено";

    return errors;
}

const IdeasCreateForm = ({
    isSubmitting,
    setBackgroundBase64,
    onCreateSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{name: "", content: ""}}
        validate={validate}
        onSubmit={onCreateSubmit}
    >
        {() => (
            <Form className={classes.ideasCreateForm}>
                <span className={classes.ideasCreateFormTitle}>Создание идеи</span>
                <IdeasCreateField id="ideas-create-name" name="name" label="Название идеи" />
                <IdeasCreateField id="ideas-create-content" name="content" label="Описание идеи" />
                <IdeasCreateImageUploadContainer setBackgroundBase64={setBackgroundBase64} />
                <IdeasCreateButtons isSubmitting={isSubmitting} redirectToMainHandle={redirectToMainHandle} />
            </Form>
        )}
    </Formik>
)

export default IdeasCreateForm;
