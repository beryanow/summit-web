import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import IdeasCreate from './IdeasCreate';


class IdeasCreateContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            redirectToMain: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {ideasInfo} = this.props;
        const {dataChanged, error} = ideasInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.createErrorHandle();
    }

    createSuccessHandle = () => {
        this.props.ideaChangeHandle();
        this.setState({isSubmitting: false});

        toast.success("Идея успешно создана!");
    }

    createErrorHandle = () => {
        this.props.ideaChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при создании вашей идеи!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onCreateSubmit = values => {
        const {name, content} = values;
        const {backgroundBase64} = this.state;

        const {authInfo} = this.props;
        const {userId, token} = authInfo;

        const size = content.length >= 20 ? "2x2" : "2x1";

        const createdIdea = {
            name,
            content,
            size,
            backgroundId: 1,
            background: backgroundBase64,
            userId
        };

        this.props.createIdea(createdIdea, token);
        this.setState({isSubmitting: true});
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {isSubmitting, redirectToMain} = this.state;
        if (redirectToMain) return <Redirect push to="/ideas" />;

        const ideasCreateProps = {
            isSubmitting: isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            onCreateSubmit: this.onCreateSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        };
        return <IdeasCreate {...ideasCreateProps} />;
    }
}

export default IdeasCreateContainer;