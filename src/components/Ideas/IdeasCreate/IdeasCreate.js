import React from 'react';

import IdeasCreateForm from './IdeasCreateForm/IdeasCreateForm';
import classes from './IdeasCreate.module.css';


const IdeasCreate = (props) => {
    return (
        <div className={classes.ideasCreate}>
            <IdeasCreateForm {...props} />
        </div>
    );
}

export default IdeasCreate;