import { connect } from 'react-redux';

import Ideas from './Ideas';
import {
    getIdeasFetch,
    createIdeaFetch,
    updateIdeaFetch,
    deleteIdeaFetch,
    ideaChangeHandle
} from '../../actions/ideaActionCreator';


const mapStateToProps =  ({ideasInfo, backgroundsInfo, authInfo}) => ({
    ideasInfo,
    backgroundsInfo,
    authInfo
})

const mapDispatchToProps = dispatch => ({
    getIdeas: (backgrounds, token) => dispatch(getIdeasFetch(backgrounds, token)),
    createIdea: (idea, token) => dispatch(createIdeaFetch(idea, token)),
    updateIdea: (idea, token) => dispatch(updateIdeaFetch(idea, token)),
    deleteIdea: (id, token) => dispatch(deleteIdeaFetch(id, token)),
    ideaChangeHandle: () => dispatch(ideaChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Ideas);
