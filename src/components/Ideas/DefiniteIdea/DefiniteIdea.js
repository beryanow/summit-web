import React from 'react';
import classes from './DefiniteIdea.module.css';


const DefiniteIdea = ({ innerComponent, background }) => {
    const backgroundImageStyle = {backgroundImage: `url(data:image/png;base64,${background})`};

    return (
        <div className={classes.definiteIdea} style={backgroundImageStyle}>
            {innerComponent}
        </div>
    );
}

export default DefiniteIdea;
