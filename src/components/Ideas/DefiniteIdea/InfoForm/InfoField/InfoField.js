import React from 'react';
import classes from './InfoField.module.css';


const InfoField = ({fieldName}) => (
    <div className={classes.infoField}>
        {fieldName}
    </div>
);

export default InfoField;

