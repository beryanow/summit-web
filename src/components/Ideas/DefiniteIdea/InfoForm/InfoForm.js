import React from 'react';
import { Formik, Form } from 'formik';

import InfoField from './InfoField/InfoField';
import InfoButtons from './InfoButtons/InfoButtons';

import classes from './InfoForm.module.css';


const InfoForm = ({
    isSubmitting,
    name,
    content,
    onDeleteSubmit,
    editingHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{}}
        onSubmit={onDeleteSubmit}
    >
        {() => (
            <Form className={classes.infoForm}>
                <span className={classes.infoFormTitle}>Идея</span>
                <InfoField fieldName={name} />
                <InfoField fieldName={content} />
                <InfoButtons
                    isSubmitting={isSubmitting}
                    editingHandle={editingHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default InfoForm;