import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import DefiniteIdea from './DefiniteIdea';
import InfoForm from './InfoForm/InfoForm';
import EditingForm from './EditingForm/EditingForm';


class DefiniteIdeaContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            isEditing: false,
            deleted: false,
            redirectToMain: false
        }
    }

    componentDidMount() {
        const {ideasInfo, backgroundsInfo, authInfo} = this.props;
        if (ideasInfo.ideas.length === 0) {
            const {backgrounds} = backgroundsInfo;
            const {token} = authInfo;

            this.props.getIdeas(backgrounds, token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {ideasInfo} = this.props;
        const {
            dataChanged,
            dataDeleted,
            error
        } = ideasInfo;

        if (dataChanged) this.updateSuccessHandle();
        else if (dataDeleted) this.deleteSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    updateSuccessHandle = () => {
        this.props.ideaChangeHandle();
        this.setState({
           isSubmitting: false,
           isEditing: false
        });

        toast.success("Идея успешна обновлена!");
    }

    deleteSuccessHandle = () => {
        this.props.ideaChangeHandle();
        this.setState({
            isSubmitting: false,
            deleted: true
        });

        toast.success("Идея удалена успешно!");
    }

    operationErrorHandle = () => {
        this.props.ideaChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при выполнении операции!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onUpdateSubmit = values => {
        const {name, content} = values;
        const {backgroundBase64} = this.state;

        const {idea, authInfo} = this.props;
        const {id, backgroundId} = idea;
        const {userId, token} = authInfo;

        const size = content.length >= 20 ? "2x2" : "2x1";

        const updatedIdea = {
            id,
            name,
            content,
            backgroundId,
            background: backgroundBase64,
            size,
            userId
        };

        this.props.updateIdea(updatedIdea, token);
        this.setState({isSubmitting: true});
    }

    onDeleteSubmit = () => {
        const {idea, authInfo} = this.props;
        const {id} = idea;
        const {token} = authInfo;

        this.props.deleteIdea(id, token);
        this.setState({isSubmitting: true});
    }

    editingHandle = () => {
        const {isEditing} = this.state;
        this.setState({isEditing: !isEditing});
    }

    cancelHandle = () => {
        this.setState({
            backgroundBase64: "",
            isEditing: false
        });
    }

    redirectToMainChange = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {
            isSubmitting,
            isEditing,
            deleted,
            redirectToMain
        } = this.state;
        if (deleted || redirectToMain) return <Redirect push to="/ideas" />;

        const {idea} = this.props;
        const {name, content, background} = idea;

        const editingProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            name,
            content,
            onUpdateSubmit: this.onUpdateSubmit,
            cancelHandle: this.cancelHandle,
            redirectToMainHandle: this.redirectToMainChange
        };
        const infoProps = {
            isSubmitting,
            name,
            content,
            onDeleteSubmit: this.onDeleteSubmit,
            editingHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainChange
        };

        const innerComponent = isEditing ? <EditingForm {...editingProps} /> : <InfoForm {...infoProps} />;
        return <DefiniteIdea innerComponent={innerComponent} background={background} />;
    }
}

export default DefiniteIdeaContainer;