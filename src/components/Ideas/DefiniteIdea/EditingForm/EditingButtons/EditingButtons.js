import React from 'react';
import classes from './EditingButtons.module.css';


const EditingButtons = ({
    isSubmitting,
    cancelHandle,
    redirectToMainHandle
}) => (
    <div className={classes.editingButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.editingButton}
        >
            Обновить идею
        </button>
        <button
            type="button"
            onClick={cancelHandle}
            disabled={isSubmitting}
            className={`${classes.editingButton} ${classes.cancelButton}`}
        >
            Отменить
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.editingButton}
        >
            Назад
        </button>
    </div>
);

export default EditingButtons;