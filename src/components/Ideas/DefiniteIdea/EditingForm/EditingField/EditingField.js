import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './EditingField.module.css';


const EditingField = ({id, name, label}) => (
    <div className={classes.editingField}>
        <label
            htmlFor={id}
            className={classes.editingFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.editingFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.editingFieldError}
        />
    </div>
);

export default EditingField;