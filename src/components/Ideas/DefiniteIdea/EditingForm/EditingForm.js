import React from 'react';
import { Formik, Form } from 'formik';

import EditingField from './EditingField/EditingField';
import EditingButtons from './EditingButtons/EditingButtons';
import EditingImageUploadContainer from './EditingImageUpload/EditingImageUploadContainer';

import classes from './EditingForm.module.css';


const validate = (values) => {
    const errors = {};
    const {name, content} = values;

    if (!name) errors.name = "Не заполнено";
    if (!content) errors.content = "Не заполнено";

    return errors;
}

const editingForm = ({
    isSubmitting,
    setBackgroundBase64,
    name,
    content,
    onUpdateSubmit,
    cancelHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{name: name, content: content}}
        validate={validate}
        onSubmit={onUpdateSubmit}
    >
        {() => (
            <Form className={classes.editingForm}>
                <span className={classes.editingFormTitle}>Обновление идеи</span>
                <EditingField id="ideas-update-name" name="name" label="Название идеи" />
                <EditingField id="ideas-update-content" name="content" label="Описание идеи" />
                <EditingImageUploadContainer setBackgroundBase64={setBackgroundBase64} />
                <EditingButtons
                    isSubmitting={isSubmitting}
                    cancelHandle={cancelHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default editingForm;