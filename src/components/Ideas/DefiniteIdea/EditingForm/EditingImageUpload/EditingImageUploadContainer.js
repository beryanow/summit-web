import React from 'react';
import EditingImageUpload from './EditingImageUpload';


class EditingImageUploadContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSuccess: false,
            isError: false,
            errorMessage: ""
        };
    }

    onLoad = ({ target: { files } }) => {
        const validateFile = file => {
            if (!file) return false;

            const kb256 = 262144;
            if (file.size > kb256) {
                this.setState({
                    isSuccess: false,
                    isError: true,
                    errorMessage: "Размер файла слишком большой"
                });
                return false;
            }

            if (file.type !== "image/jpeg" && file.type !== "image/jpg" && file.type !== "image/png") {
                this.setState({
                    isSuccess: false,
                    isError: true,
                    errorMessage: "Неподдерживаемый формат файла"
                });
                return false;
            }

            return true;
        }

        const onloadendFunc = () => {
            const backgroundBase64 = reader.result.replace(/^data:image.+;base64,/, '');
            this.props.setBackgroundBase64(backgroundBase64);

            this.setState({
                isSuccess: true,
                isError: false
            });
        }

        const file = files[0];
        if (!validateFile(file)) return;

        const reader = new FileReader();
        reader.addEventListener("loadend", onloadendFunc);
        reader.readAsDataURL(file);
    }

    render() {
        const {
            isSuccess,
            isError,
            errorMessage
        } = this.state;

        return (
            <EditingImageUpload
                isSuccess={isSuccess}
                isError={isError}
                errorMessage={errorMessage}
                onLoad={this.onLoad}
            />
        );
    }
}

export default EditingImageUploadContainer;
