import React from 'react';
import classes from './EditingImageUpload.module.css';


const EditingImageUpload = ({
    isSuccess,
    isError,
    errorMessage,
    onLoad
}) => {
    const additionalClassname = isSuccess ?
        classes.editingImageUploadStatusSuccess :
        isError ? classes.editingImageUploadStatusError : "";
    const statusMessage = isSuccess ? "Изображение успешно загружено" : errorMessage;

    return (
        <div className={classes.editingImageUpload}>
            <label
                htmlFor="ideas-update-upload"
                className={classes.editingImageUploadLabel}
            >
                Загрузить изображение
            </label>
            <input
                id="ideas-update-upload"
                type="file"
                onChange={onLoad}
                className={classes.editingImageUploadInput}
            />
            <div className={`${classes.editingImageUploadStatus} ${additionalClassname}`}>
                {statusMessage}
            </div>
        </div>
    );
}

export default EditingImageUpload;
