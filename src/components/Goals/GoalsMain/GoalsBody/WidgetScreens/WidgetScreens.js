import React from 'react';
import classes from './WidgetScreens.module.css';


const WidgetScreens = ({widgetScreens}) => (
    <div id="goals-widgets-screens" className={classes.widgetScreens}>
        {widgetScreens}
    </div>
);

export default WidgetScreens;