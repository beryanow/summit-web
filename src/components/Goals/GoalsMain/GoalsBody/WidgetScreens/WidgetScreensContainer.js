import React from 'react';

import WidgetScreens from './WidgetScreens';
import DefiniteScreen from './DefiniteScreen/DefiniteScreen';


class WidgetScreensContainer extends React.Component {
    createWidgetScreens = (goals) => {
        const widgetScreensObj = {
            widgetScreensArr: [],
            currentGoalIdx: 0
        };

        while (goals.length > widgetScreensObj.currentGoalIdx) {
            this.createNewDefiniteScreen(widgetScreensObj, goals);
        }

        return widgetScreensObj.widgetScreensArr;
    }

    createNewDefiniteScreen = (widgetScreensObj, goals) => {
        const screenObj = {
            screenGoals: [],
            freeCells: 16
        }

        while (screenObj.freeCells > 0 && goals.length > widgetScreensObj.currentGoalIdx) {
            const goal = goals[widgetScreensObj.currentGoalIdx];
            screenObj.freeCells -= goal.size === "2x2" ? 4 : 2;

            screenObj.screenGoals.push(goal);

            widgetScreensObj.currentGoalIdx += 1;
        }

        const definiteScreen = <DefiniteScreen key={widgetScreensObj.currentGoalIdx} screenGoals={screenObj.screenGoals} />;
        widgetScreensObj.widgetScreensArr.push(definiteScreen);
    }

    render() {
        const {goals} = this.props;
        const widgetScreensArr = this.createWidgetScreens(goals);

        return (
          <WidgetScreens widgetScreens={widgetScreensArr} />
        );
    }
}

export default WidgetScreensContainer;