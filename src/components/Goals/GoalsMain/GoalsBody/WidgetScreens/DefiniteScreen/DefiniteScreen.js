import React from 'react';

import GoalWidget from './GoalWidget/GoalWidget';
import classes from './DefiniteScreen.module.css';


const DefiniteScreen = ({screenGoals}) => {
    const widgets = screenGoals.map(goal => <GoalWidget key={goal.id} goal={goal} />);

    return (
      <div className={classes.widgetScreen}>
          {widgets}
      </div>
    );
}

export default DefiniteScreen;