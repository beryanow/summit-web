import React from 'react';
import { Link } from 'react-router-dom';

import classes from './GoalWidget.module.css';


const GoalWidget = ({goal}) => {
    const getTaskStageCSSClass = (taskStage) => {
        switch (taskStage) {
            case "New":
                return classes.redTask;
            case "In progress":
                return classes.yellowTask;
            case "Done":
                return classes.greenTask;
            default:
                return "";
        }
    }

    const taskItems = goal.tasks.map(task => {
        const taskStageCSSClass = getTaskStageCSSClass(task.stage);
        return <span key={task.id} className={`${classes.task} ${taskStageCSSClass}`}>{task.name}</span>;
    });

    const widgetBackgroundStr = goal.background ? `data:image/png;base64,${goal.background}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = goal.size === "2x2" ? classes.bigWidget : classes.middleWidget;

    const widgetLink = `/goals/${goal.id}`;

    return (
        <Link to={widgetLink} style={widgetBackgroundStyle} className={`${classes.goalWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.goalWidgetTitle}>{goal.name}</span>
            <div className={classes.goalWidgetContent}>
                {taskItems}
            </div>
        </Link>
    )
}

export default GoalWidget;