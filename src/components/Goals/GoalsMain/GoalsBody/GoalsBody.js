import React from 'react';
import {AiOutlineDoubleLeft, AiOutlineDoubleRight} from 'react-icons/all';

import WidgetScreensContainer from './WidgetScreens/WidgetScreensContainer';
import classes from './GoalsBody.module.css';


const GoalsBody = ({goals}) => {
    const goRight = () => {
        const element = document.getElementById("goals-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft + element.clientWidth,
            behavior: "smooth"
        });
    }

    const goLeft = () => {
        const element = document.getElementById("goals-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft - element.clientWidth,
            behavior: "smooth"
        });
    }

    const isMoreOnePageCalc = (goals) => {
        let summarySize = 0;

        for (const goal of goals) {
            summarySize += goal.size === "2x2" ? 4 : 2;
            if (summarySize > 16) return true;
        }

        return false;
    }


    const isMoreOnePage = isMoreOnePageCalc(goals);
    return (
        <div className={classes.goalsBody}>
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goLeft()}>
                    <AiOutlineDoubleLeft/>
                </div>
            }
            <WidgetScreensContainer goals={goals} />
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goRight()}>
                    <AiOutlineDoubleRight/>
                </div>
            }
        </div>
    );
}

export default GoalsBody;