import React from 'react';
import { toast } from 'react-toastify';

import GoalsMain from './GoalsMain';


class GoalsMainContainer extends React.Component {
    componentDidMount() {
        const {backgroundsInfo, authInfo} = this.props;
        const {backgrounds} = backgroundsInfo;
        const {token} = authInfo;

        this.props.getGoals(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {goalsInfo} = this.props;
        const {error} = goalsInfo;

        if (error) this.loadErrorHandle();
    }

    loadErrorHandle = () => {
        this.props.goalChangeHandle();
        toast.error("Не удалось загрузить данные!");
    }

    render() {
        const {goalsInfo} = this.props;
        const {goals} = goalsInfo;

        goals.sort((first, second) => {
            const firstTasksCount = first.tasks.length;
            const secondTasksCount = second.tasks.length;

            if (firstTasksCount > secondTasksCount) return -1;
            else if (firstTasksCount < secondTasksCount) return 1;
            else return 0;
        });

        return <GoalsMain goals={goals} />;
    }
}

export default GoalsMainContainer;
