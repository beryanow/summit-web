import React from 'react';
import {FiTarget} from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import GoalsBody from './GoalsBody/GoalsBody';

import classes from './GoalsMain.module.css';


const GoalsMain = ({goals}) => {
    const headerText = "#цели";
    const headerIcon = <FiTarget />;
    const footerLink = "/goals/create";

    return (
        <div className={classes.goals}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <GoalsBody goals={goals} />
            <SectionFooter createLink={footerLink} />
        </div>
    );
}

export default GoalsMain;