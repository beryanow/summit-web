import React from 'react';
import { Switch, Route } from 'react-router-dom';

import GoalsMainContainer from './GoalsMain/GoalsMainContainer';
import GoalsCreateContainer from './GoalsCreate/GoalsCreateContainer';
import DefiniteGoalContainer from './DefiniteGoal/DefiniteGoalContainer';


const Goals = (props) => {
    const searchGoalById = (id) => {
        for (const goal of goalsInfo.goals) {
            if (goal.id === Number.parseInt(id)) return goal;
        }

        const placeholderGoal = {name: "", description: "", tasks: []};
        return placeholderGoal;
    }

    const {goalsInfo, backgroundsInfo, authInfo} = props;
    const {getGoals, createGoal, updateGoal, deleteGoal, goalChangeHandle} = props;

    const goalsMainProps = {goalsInfo, backgroundsInfo, authInfo, getGoals, goalChangeHandle};
    const goalsCreateProps = {goalsInfo, authInfo, createGoal, goalChangeHandle};
    const definiteGoalProps = {goalsInfo, backgroundsInfo, authInfo, getGoals, updateGoal, deleteGoal, goalChangeHandle};

    return (
        <Switch>
            <Route exact path="/goals" render={() => <GoalsMainContainer {...goalsMainProps} />} />
            <Route exact path="/goals/create" render={() => <GoalsCreateContainer {...goalsCreateProps} />} />
            <Route exact path="/goals/:id" render={(props) => <DefiniteGoalContainer {...definiteGoalProps} goal={searchGoalById(props.match.params.id)} />} />
        </Switch>
    );
}

export default Goals;