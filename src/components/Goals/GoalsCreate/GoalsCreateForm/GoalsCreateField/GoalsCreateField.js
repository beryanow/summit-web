import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './GoalsCreateField.module.css';


const GoalsCreateField = ({ id, name, label }) => (
    <div className={classes.goalsCreateField}>
        <label
            htmlFor={id}
            className={classes.goalsCreateFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.goalsCreateFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.goalsCreateFieldError}
        />
    </div>
);

export default GoalsCreateField;