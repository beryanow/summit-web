import React from 'react';
import { Formik, Form } from 'formik';

import GoalsCreateField from './GoalsCreateField/GoalsCreateField';
import GoalsCreateFieldArray from './GoalsCreateFieldArray/GoalsCreateFieldArray';
import GoalsCreateButtons from './GoalsCreateButtons/GoalsCreateButtons';
import GoalsCreateImageUploadContainer from './GoalsCreateImageUpload/GoalsCreateImageUploadContainer';

import classes from './GoalsCreateForm.module.css';


const validate = (values) => {
    const validateTasksArr = tasks => {
        const tasksError = [];
        let isErrorsExist = false;

        tasks.forEach((task) => {
            const {name, description, stage} = task;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!description) errorObject.description = "Не заполнено";


            const stageStatuses = ["New", "In progress", "Done"];
            const stageIsValid = stageStatuses.some(stageStatus => stageStatus === stage);
            if (!stage || !stageIsValid) errorObject.stage = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            tasksError.push(errorObject);
        });

        return isErrorsExist ? tasksError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);

    for (const [key, value] of valuesArr) {
        if (key === "tasks") {
            const tasksError = validateTasksArr(value);
            if (tasksError) errors.tasks = tasksError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const GoalsCreateForm = ({
    isSubmitting,
    setBackgroundBase64,
    onCreateSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{
            name: "",
            description: "",
            tasks: []
        }}
        validate={validate}
        onSubmit={onCreateSubmit}
    >
        {({values}) => (
            <Form className={classes.goalsCreateForm}>
                <span className={classes.goalsCreateFormTitle}>Создание цели</span>
                <GoalsCreateField
                    id="goals-create-name"
                    name="name"
                    label="Название цели"
                />
                <GoalsCreateField
                    id="goals-create-description"
                    name="description"
                    label="Описание цели"
                />
                <GoalsCreateImageUploadContainer
                    setBackgroundBase64={setBackgroundBase64}
                />
                <GoalsCreateFieldArray
                    isSubmitting={isSubmitting}
                    tasks={values.tasks}
                />
                <GoalsCreateButtons
                    isSubmitting={isSubmitting}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default GoalsCreateForm;