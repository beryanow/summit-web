import React from 'react';
import { Field, ErrorMessage, FieldArray } from 'formik';

import TaskField from './TaskField/TaskField';
import classes from './GoalsCreateFieldArray.module.css';


const GoalsCreateFieldArray = ({ isSubmitting, tasks }) => (
    <FieldArray name="tasks" >
        {({ push, remove }) => (
            <div className={classes.goalsCreateFieldArray}>
                {
                    tasks.length > 0
                    &&
                    <span className={classes.goalsCreateFieldArrayTitle}>Задачи</span>
                }
                {tasks.map((task, index) => (
                    <div key={index} className={classes.goalsCreateFieldArrayElement}>
                        <TaskField
                            id={`tasks-create-name-${index}`}
                            name={`tasks[${index}].name`}
                            label="Название задачи"
                        />
                        <TaskField
                            id={`tasks-create-description-${index}`}
                            name={`tasks[${index}].description`}
                            label="Описание задачи"
                        />
                        <Field
                            name={`tasks[${index}].stage`}
                            component="select"
                            className={classes.goalsCreateFieldArrayElementSelect}
                        >
                            <option defaultValue={true}>Выберите стадию</option>
                            <option value="New">New</option>
                            <option value="In progress">In progress</option>
                            <option value="Done">Done</option>
                        </Field>
                        <ErrorMessage
                            name={`tasks[${index}].stage`}
                            component="div"
                            className={classes.goalsCreateFieldArrayElementSelectError}
                        />
                        <button
                            type="button"
                            onClick={() => remove(index)}
                            disabled={isSubmitting}
                        >
                            Удалить
                        </button>
                    </div>
                ))}
                <button
                    type="button"
                    onClick={() => push({name: "", description: "", stage: ""})}
                    disabled={isSubmitting}
                >
                    Добавить задачу
                </button>
            </div>
        )}
    </FieldArray>
);

export default GoalsCreateFieldArray;