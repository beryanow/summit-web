import React from 'react';
import classes from './GoalsCreateImageUpload.module.css';


const GoalsCreateImageUpload = ({
    isSuccess,
    isError,
    errorMessage,
    onLoad
}) => {
    const additionalClassname = isSuccess ?
        classes.goalsCreateImageUploadStatusSuccess :
        isError ? classes.goalsCreateImageUploadStatusError : "";
    const statusMessage = isSuccess ? "Изображение успешно загружено" : errorMessage;

    return (
        <div className={classes.goalsCreateImageUpload}>
            <label
                htmlFor="goals-create-upload"
                className={classes.goalsCreateImageUploadLabel}
            >
                Загрузить изображение
            </label>
            <input
                id="goals-create-upload"
                type="file"
                onChange={onLoad}
                className={classes.goalsCreateImageUploadInput}
            />
            <div className={`${classes.goalsCreateImageUploadStatus} ${additionalClassname}`}>
                {statusMessage}
            </div>
        </div>
    );
}

export default GoalsCreateImageUpload;
