import React from 'react';
import classes from './GoalsCreateButtons.module.css';


const GoalsCreateButtons = ({ isSubmitting, redirectToMainHandle }) => (
    <div className={classes.goalsCreateButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.goalsCreateButton}
        >
            Создать цель
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.goalsCreateButton}
        >
            Назад
        </button>
    </div>
);

export default GoalsCreateButtons;