import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import GoalsCreate from './GoalsCreate';


class GoalsCreateContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            redirectToMain: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {goalsInfo} = this.props;
        const {dataChanged, error} = goalsInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.createErrorHandle();
    }

    createSuccessHandle = () => {
        this.props.goalChangeHandle();
        this.setState({isSubmitting: false});

        toast.success("Цель успешно создана!");
    }

    createErrorHandle = () => {
        this.props.goalChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при создании вашей цели!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onCreateSubmit = values => {
        const {name, description, tasks} = values;
        const {backgroundBase64} = this.state;

        const {authInfo} = this.props;
        const {userId, token} = authInfo;

        const tasksForFetch = tasks.map(task => ({...task, userId}));
        const size = tasks.length >= 2 ? "2x2" : "2x1";

        const createdGoal = {
            name,
            description,
            tasks: tasksForFetch,
            size,
            statePlace: "Not implemented",
            backgroundId: 1,
            background: backgroundBase64,
            userId
        };

        this.props.createGoal(createdGoal, token);
        this.setState({isSubmitting: true});
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {isSubmitting, redirectToMain} = this.state;
        if (redirectToMain) return <Redirect push to="/goals" />;

        const goalsCreateProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            onCreateSubmit: this.onCreateSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        };
        return <GoalsCreate {...goalsCreateProps} />;
    }
}

export default GoalsCreateContainer;