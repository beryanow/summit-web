import React from 'react';

import GoalsCreateForm from './GoalsCreateForm/GoalsCreateForm';
import classes from './GoalsCreate.module.css';


const GoalsCreate = (props) => (
    <div className={classes.goalsCreate}>
        <GoalsCreateForm {...props} />
    </div>
);

export default GoalsCreate;