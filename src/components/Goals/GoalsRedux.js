import { connect } from 'react-redux';

import Goals from './Goals';
import {
    getGoalsFetch,
    createGoalFetch,
    updateGoalFetch,
    deleteGoalFetch,
    goalChangeHandle
} from '../../actions/goalActionCreator';


const mapStateToProps = ({goalsInfo, backgroundsInfo, authInfo}) => ({
    goalsInfo,
    backgroundsInfo,
    authInfo
})

const mapDispatchToProps = dispatch => ({
    getGoals: (backgrounds, token) => dispatch(getGoalsFetch(backgrounds, token)),
    createGoal: (goal, token) => dispatch(createGoalFetch(goal, token)),
    updateGoal: (goal, token) => dispatch(updateGoalFetch(goal, token)),
    deleteGoal: (id, token) => dispatch(deleteGoalFetch(id, token)),
    goalChangeHandle: () => dispatch(goalChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Goals);