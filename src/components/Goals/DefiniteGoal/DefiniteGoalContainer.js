import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import InfoForm from './InfoForm/InfoForm';
import EditingForm from './EditingForm/EditingForm';
import DefiniteGoal from './DefiniteGoal';


class DefiniteGoalContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            isEditing: false,
            deleted: false,
            redirectToMain: false
        };
    }

    componentDidMount() {
        const {goalsInfo, backgroundsInfo, authInfo} = this.props;
        if (goalsInfo.goals.length === 0) {
            const {backgrounds} = backgroundsInfo;
            const {token} = authInfo;

            this.props.getGoals(backgrounds, token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {goalsInfo} = this.props;
        const {
            dataChanged,
            dataDeleted,
            error
        } = goalsInfo;

        if (dataChanged) this.updateSuccessHandle();
        else if (dataDeleted) this.deleteSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    updateSuccessHandle = () => {
        this.props.goalChangeHandle();
        this.setState({
            isSubmitting: false,
            isEditing: false
        });

        toast.success("Цель успешно обновлена!");
    }

    deleteSuccessHandle = () => {
        this.props.goalChangeHandle();
        this.setState({
            isSubmitting: false,
            deleted: true
        });

        toast.success("Цель удалена успешно!");
    }

    operationErrorHandle = () => {
        this.props.goalChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при выполнении операции!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onUpdateSubmit = values => {
        const {name, description, tasks} = values;
        const {backgroundBase64} = this.state;

        const {goal, authInfo} = this.props;
        const {id, backgroundId} = goal;
        const {userId, token} = authInfo;

        const tasksForFetch = tasks.map(task => ({...task, userId}));
        const size = tasks.length >= 2 ? "2x2" : "2x1";

        const updatedGoal = {
            id,
            name,
            description,
            tasks: tasksForFetch,
            statePlace: "Not implemented",
            backgroundId,
            background: backgroundBase64,
            size,
            userId
        };

        this.props.updateGoal(updatedGoal, token);
        this.setState({isSubmitting: true});
    }

    onDeleteSubmit = () => {
        const {goal, authInfo} = this.props;
        const {id} = goal;
        const {token} = authInfo;

        this.props.deleteGoal(id, token);
        this.setState({isSubmitting: true});
    }

    editingHandle = () => {
        const {isEditing} = this.state;
        this.setState({
            backgroundBase64: "",
            isEditing: !isEditing
        });
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {
            isSubmitting,
            isEditing,
            deleted,
            redirectToMain
        } = this.state;
        if (deleted || redirectToMain) return <Redirect push to="/goals" />;

        const {goal} = this.props;
        const {name, description, tasks, background} = goal;

        const editingProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            initialValues: {name, description, tasks},
            onUpdateSubmit: this.onUpdateSubmit,
            cancelHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const infoProps = {
            isSubmitting,
            name,
            description,
            tasks,
            onDeleteSubmit: this.onDeleteSubmit,
            editingHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };

        const innerComponent = isEditing ? <EditingForm {...editingProps} /> : <InfoForm {...infoProps} />;
        return <DefiniteGoal innerComponent={innerComponent} background={background} />;
    }
}

export default DefiniteGoalContainer;