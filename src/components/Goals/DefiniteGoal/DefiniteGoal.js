import React from 'react';
import classes from './DefiniteGoal.module.css';


const DefiniteGoal = ({ innerComponent, background }) => {
    const backgroundImageStyle = {backgroundImage: `url(data:image/png;base64,${background})`};

    return (
        <div className={classes.definiteGoal} style={backgroundImageStyle}>
            {innerComponent}
        </div>
    );
};

export default DefiniteGoal;
