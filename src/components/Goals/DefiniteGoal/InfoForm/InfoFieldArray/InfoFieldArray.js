import React from 'react';
import classes from './InfoFieldArray.module.css';


const InfoFieldArray = ({tasks}) => (
    <div className={classes.infoFieldArray}>
        {
            tasks.length > 0
            &&
            <span className={classes.infoFieldArrayTitle}>Задачи</span>
        }
        {tasks.map((task, index) => (
            <div key={index} className={classes.infoFieldArrayElement}>
                <span className={classes.infoFieldArrayElementTitle}>
                    {task.name}
                </span>
                <div className={classes.infoFieldArrayElementDescription}>
                    {task.description}
                </div>
            </div>
        ))}
    </div>
);

export default InfoFieldArray;
