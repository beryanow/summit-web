import React from 'react';
import { Formik, Form } from 'formik';

import InfoField from './InfoField/InfoField';
import InfoFieldArray from './InfoFieldArray/InfoFieldArray';
import InfoButtons from './InfoButtons/InfoButtons';

import classes from './InfoForm.module.css';


const InfoForm = ({
    isSubmitting,
    name,
    description,
    tasks,
    onDeleteSubmit,
    editingHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{}}
        onSubmit={onDeleteSubmit}
    >
        {() => (
            <Form className={classes.infoForm}>
                <span className={classes.infoFormTitle}>Цель</span>
                <InfoField value={name} />
                <InfoField value={description} />
                <InfoFieldArray tasks={tasks} />
                <InfoButtons
                    isSubmitting={isSubmitting}
                    editingHandle={editingHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default InfoForm;