import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './TaskField.module.css';


const TaskField = ({ id, name, label }) => (
    <div className={classes.taskField}>
        <label
            htmlFor={id}
            className={classes.taskFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.taskFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.taskFieldError}
        />
    </div>
);

export default TaskField;