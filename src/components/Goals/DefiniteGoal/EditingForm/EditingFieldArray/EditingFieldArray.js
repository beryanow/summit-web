import React from 'react';
import { Field, ErrorMessage, FieldArray } from 'formik';

import TaskField from './TaskField/TaskField';
import classes from './EditingFieldArray.module.css';


const EditingFieldArray = ({ isSubmitting, tasks }) => (
    <FieldArray name="tasks" >
        {({ push, remove }) => (
            <div className={classes.editingFieldArray}>
                {
                    tasks.length > 0
                    &&
                    <span className={classes.editingFieldArrayTitle}>Задачи</span>
                }
                {tasks.map((task, index) => (
                    <div key={index} className={classes.editingFieldArrayElement}>
                        <TaskField
                            id={`tasks-update-name-${index}`}
                            name={`tasks[${index}].name`}
                            label="Название задачи"
                        />
                        <TaskField
                            id={`tasks-update-description-${index}`}
                            name={`tasks[${index}].description`}
                            label="Описание задачи"
                        />
                        <Field
                            name={`tasks[${index}].stage`}
                            component="select"
                            className={classes.editingFieldArrayElementSelect}
                        >
                            <option
                                defaultValue={task.stage === "New"}
                                value="New"
                            >
                                New
                            </option>
                            <option
                                defaultValue={task.stage === "In progress"}
                                value="In progress"
                            >
                                In progress
                            </option>
                            <option
                                defaultValue={task.stage === "Done"}
                                value="Done"
                            >
                                Done
                            </option>
                        </Field>
                        <ErrorMessage
                            name={`tasks[${index}].stage`}
                            component="div"
                            className={classes.editingFieldArrayElementSelectError}
                        />
                        <button
                            type="button"
                            onClick={() => remove(index)}
                            disabled={isSubmitting}
                        >
                            Удалить
                        </button>
                    </div>
                ))}
                <button
                    type="button"
                    onClick={() => push({name: "", description: "", stage: ""})}
                    disabled={isSubmitting}
                >
                    Добавить задачу
                </button>
            </div>
        )}
    </FieldArray>
);

export default EditingFieldArray;