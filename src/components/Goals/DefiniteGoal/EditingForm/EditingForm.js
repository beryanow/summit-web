import React from 'react';
import { Formik, Form } from 'formik';

import EditingField from './EditingField/EditingField';
import EditingFieldArray from './EditingFieldArray/EditingFieldArray';
import EditingButtons from './EditingButtons/EditingButtons';
import EditingImageUploadContainer from './EditingImageUpload/EditingImageUploadContainer';

import classes from './EditingForm.module.css';


const validate = (values) => {
    const validateTasksArr = tasks => {
        const tasksError = [];
        let isErrorsExist = false;

        tasks.forEach((task) => {
            const {name, description, stage} = task;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!description) errorObject.description = "Не заполнено";
            if (!stage) errorObject.stage = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            tasksError.push(errorObject);
        });

        return isErrorsExist ? tasksError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);

    for (const [key, value] of valuesArr) {
        if (key === "tasks") {
            const tasksError = validateTasksArr(value);
            if (tasksError) errors.tasks = tasksError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const EditingForm = ({
    isSubmitting,
    setBackgroundBase64,
    initialValues,
    onUpdateSubmit,
    cancelHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={onUpdateSubmit}
    >
        {({values}) => (
            <Form className={classes.editingForm}>
                <span className={classes.editingFormTitle}>Обновление цели</span>
                <EditingField
                    id="goals-update-name"
                    name="name"
                    label="Название цели"
                />
                <EditingField
                    id="goals-update-description"
                    name="description"
                    label="Описание цели"
                />
                <EditingImageUploadContainer
                    setBackgroundBase64={setBackgroundBase64}
                />
                <EditingFieldArray
                    isSubmitting={isSubmitting}
                    tasks={values.tasks}
                />
                <EditingButtons
                    isSubmitting={isSubmitting}
                    cancelHandle={cancelHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default EditingForm;
