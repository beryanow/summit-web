import { connect } from 'react-redux';

import Start from './Start';
import {
    signInFetch,
    signUpFetch,
    authChangeHandle
} from '../../actions/authActionCreator';


const mapStateToProps = ({ authInfo }) => ({
    authInfo
})

const mapDispatchToProps = dispatch => ({
    signIn: (username, password) => dispatch(signInFetch(username, password)),
    signUp: (dataForSignUp) => dispatch(signUpFetch(dataForSignUp)),
    authChangeHandle: () => dispatch(authChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Start);