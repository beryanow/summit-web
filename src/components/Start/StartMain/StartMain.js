import React from 'react';

import SectionHeader from '../../SectionHeader/SectionHeader';
import StartBody from './StartBody/StartBody';
import LoginButtons from './LoginButtons/LoginButtons';

import classes from './StartMain.module.css';


const StartMain = ({ background, slide }) => {
    const {imageSrc, citation} = background;

    const headerText = "#summit";
    const headerIcon = <div className={classes.rocketLogo} />;

    return (
        <div className={classes.startContainer} style={{backgroundImage: `url(${imageSrc})`}}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <StartBody citation={citation} slide={slide} />
            <LoginButtons />
        </div>
    );
}

export default StartMain;