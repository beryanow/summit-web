import React from 'react';
import TextTransition from 'react-text-transition';
import { FaArrowLeft, FaArrowRight } from 'react-icons/all';

import classes from './StartBody.module.css';


const StartBody = ({ citation, slide }) => {
    return (
        <div className={classes.startBody}>
            <span onClick={() => slide("left")}>
                <FaArrowLeft />
            </span>
            <div>
                <TextTransition text={citation} />
            </div>
            <span onClick={() => slide("right")}>
                <FaArrowRight />
            </span>
        </div>
    );
}

export default StartBody;