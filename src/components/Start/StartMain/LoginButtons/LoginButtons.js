import React from 'react';
import {NavLink} from 'react-router-dom';

import classes from './LoginButtons.module.css';


const LoginButtons = () => {
    const signInIcon = <div className={classes.signInLogo} />;
    const signUpIcon = <div className={classes.signUpLogo} />;

    return (
        <div className={classes.loginButtons}>
            <div className={classes.serviceNavigation}>
                <NavLink to="/signUp" className={classes.navItem}>
                    {signUpIcon}
                    {"#регистрация"}
                </NavLink>
                <NavLink to="/signIn" className={classes.navItem}>
                    {signInIcon}
                    {"#вход"}
                </NavLink>
            </div>
        </div>
    );
};

export default LoginButtons;