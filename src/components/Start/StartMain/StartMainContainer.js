import React from 'react';
import StartMain from './StartMain';


const initialState = {
    currentBackgroundIdx: 0,
    backgrounds: [
        {imageSrc: "images/start/startBg1.jpg", citation: "Успех не всегда связан с величием, успех - это про последовательность"},
        {imageSrc: "images/start/startBg2.jpg", citation: "Если человек встает после падения,это не физика, это характер"}
    ]
};

class StartMainContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    slide = (direction) => {
        const {currentBackgroundIdx, backgrounds} = this.state;

        switch (direction) {
            case 'left':
                const leftValue = (currentBackgroundIdx - 1 + backgrounds.length) % backgrounds.length;
                this.setState({ currentBackgroundIdx: leftValue });
                break;
            case 'right':
                const rightValue = (currentBackgroundIdx + 1 + backgrounds.length) % backgrounds.length;
                this.setState({ currentBackgroundIdx: rightValue });
                break;
            default:
                break;
        }
    }

    render() {
        const {currentBackgroundIdx, backgrounds} = this.state;
        const background = backgrounds[currentBackgroundIdx];

        return <StartMain background={background} slide={this.slide} />;
    }
}

export default StartMainContainer;