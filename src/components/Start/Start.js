import React from 'react';
import { Switch, Route } from 'react-router-dom';

import StartMainContainer from './StartMain/StartMainContainer';
import SignInContainer from './SignIn/SignInContainer';
import SignUpContainer from './SignUp/SignUpContainer';


const Start = (props) => {
    const {authInfo} = props;
    const {signIn, signUp, authChangeHandle} = props;

    const signInProps = {authInfo, signIn, authChangeHandle};
    const signUpProps = {authInfo, signUp, authChangeHandle};

    return (
        <Switch>
            <Route exact path="/" component={StartMainContainer} />
            <Route exact path="/signIn" render={() => <SignInContainer {...signInProps} />} />
            <Route exact path="/signUp" render={() => <SignUpContainer {...signUpProps} />} />
        </Switch>
    );
}

export default Start;