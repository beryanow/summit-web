import React from 'react';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SignInForm from './SignInForm/SignInForm';
import classes from './SignIn.module.css';


const SignIn = (props) => {
    const headerText = "#summit";
    const headerIcon = <div className={classes.rocketLogo} />;

    return (
        <div className={classes.signIn}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <SignInForm {...props} />
        </div>
    );
};

export default SignIn;
