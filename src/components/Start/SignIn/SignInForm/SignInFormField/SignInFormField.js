import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './SignInFormField.module.css';


const SignInFormField = ({ id, name, label }) => (
    <div className={classes.signInFormField}>
        <label
            htmlFor={id}
            className={classes.signInFormFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type={name === "password" ? "password" : "text"}
            name={name}
            autoComplete="off"
            className={classes.signInFormFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.signInFormFieldError}
        />
    </div>
);

export default SignInFormField;
