import React from 'react';
import { Formik, Form } from 'formik';

import SignInFormField from './SignInFormField/SignInFormField';
import SignInFormButtons from './SignInFormButtons/SignInFormButtons';

import classes from './SignInForm.module.css';


const validate = values => {
    const errors = {};
    const {username, password} = values;

    if (!username) errors.username = "Не заполнено";
    if (!password) errors.password = "Не заполнено";

    return errors;
}

const SignInForm = ({
    isSubmitting,
    onSignInSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{ username: "", password: "" }}
        validate={validate}
        onSubmit={onSignInSubmit}
    >
        {() => (
            <Form className={classes.signInForm}>
                <span className={classes.signInFormTitle}>Вход</span>
                <SignInFormField
                    id="sign-in-username"
                    name="username"
                    label="Имя пользователя"
                />
                <SignInFormField
                    id="sign-in-password"
                    name="password"
                    label="Пароль"
                />
                <SignInFormButtons
                    isSubmitting={isSubmitting}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default SignInForm;
