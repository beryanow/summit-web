import React from 'react';
import classes from './SignInFormButtons.module.css';


const SignInFormButtons = ({ isSubmitting, redirectToMainHandle }) => (
    <div className={classes.signInFormButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.signInFormButton}
        >
            Войти
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.signInFormButton}
        >
            Назад
        </button>
    </div>
);

export default SignInFormButtons;
