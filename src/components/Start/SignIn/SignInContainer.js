import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import SignIn from './SignIn';


class SignInContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            redirectToGoals: false,
            redirectToMain: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {authInfo} = this.props;
        const {isSuccess, error} = authInfo;

        if (isSuccess) this.successHandle();
        else if (error) this.errorHandle();
    }

    successHandle = () => {
        this.props.authChangeHandle();
        this.setState({ redirectToGoals: true });

        toast.success("Вход произведен успешно!");
    }

    errorHandle = () => {
        this.props.authChangeHandle();
        this.setState({ isSubmitting: false });

        toast.error("Ошибка при входе!");
    }

    redirectToMainHandle = () => this.setState({ redirectToMain: true });

    onSignInSubmit = values => {
        const { username, password } = values;
        this.props.signIn(username, password);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            isSubmitting,
            redirectToGoals,
            redirectToMain
        } = this.state;

        if (redirectToGoals) return <Redirect push to="/goals" />;
        else if (redirectToMain) return <Redirect push to="/" />;

        const signInProps = {
            isSubmitting,
            onSignInSubmit: this.onSignInSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        };
        return <SignIn {...signInProps} />;
    }
}

export default SignInContainer;
