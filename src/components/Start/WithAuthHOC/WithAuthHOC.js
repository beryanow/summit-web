import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';


/*
 HOC, который проверяет, авторизован ли пользователь,
 и в случае отрицательного ответа возвращает на страницу логина
 */
const WithAuthHOC = (WrappedComponent) => {
    const classHoc = class HOC extends React.Component {
        render() {
            if (!this.props.token) return <Redirect push to="/signIn" />;
            return <WrappedComponent />;
        }
    };

    const toRedux = (classHoc) => {
        const mapStateToProps = ({ authInfo }) => ({ token: authInfo.token });
        return connect(mapStateToProps)(classHoc);
    };

    return toRedux(classHoc);
}

export default WithAuthHOC;
