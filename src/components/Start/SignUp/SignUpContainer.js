import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import SignUp from './SignUp';


class SignUpContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            redirectToSignIn: false,
            redirectToMain: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {authInfo} = this.props;
        const {isSuccess, error} = authInfo;

        if (isSuccess) this.successHandle();
        else if (error) this.errorHandle();
    }

    successHandle = () => {
        this.props.authChangeHandle();
        this.setState({ redirectToSignIn: true });

        toast.success("Регистрация произведена успешно!");
    }

    errorHandle = () => {
        this.props.authChangeHandle();
        this.setState({ isSubmitting: false });

        toast.error("Ошибка при регистрации!");
    }

    redirectToMainHandle = () => this.setState({ redirectToMain: true });

    onSignUpSubmit = values => {
        const updatedValues = {...values, age: Number.parseInt(values.age)};
        this.props.signUp(updatedValues);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            isSubmitting,
            redirectToSignIn,
            redirectToMain
        } = this.state;

        if (redirectToSignIn) return <Redirect push to="/signIn" />;
        else if (redirectToMain) return <Redirect push to="/" />;

        const signUpProps = {
            isSubmitting,
            onSignUpSubmit: this.onSignUpSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        };
        return <SignUp {...signUpProps} />;
    }
}

export default SignUpContainer;
