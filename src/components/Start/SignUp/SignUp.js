import React from 'react';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SignUpForm from './SignUpForm/SignUpForm';
import classes from './SignUp.module.css';


const SignUp = (props) => {
    const headerText = "#summit";
    const headerIcon = <div className={classes.rocketLogo} />;

    return (
        <div className={classes.signUp}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <SignUpForm {...props} />
        </div>
    );
};

export default SignUp;
