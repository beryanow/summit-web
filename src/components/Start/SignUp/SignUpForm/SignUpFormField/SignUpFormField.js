import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './SignUpFormField.module.css';


const SignUpFormField = ({ id, name, label }) => (
    <div className={classes.signUpFormField}>
        <label
            htmlFor={id}
            className={classes.signUpFormFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type={name === "password" ? "password" : "text"}
            name={name}
            autoComplete="off"
            className={classes.signUpFormFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.signUpFormFieldError}
        />
    </div>
);

export default SignUpFormField;
