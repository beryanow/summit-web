import React from 'react';
import classes from './SignUpFormButtons.module.css';


const SignUpFormButtons = ({ isSubmitting, redirectToMainHandle }) => (
    <div className={classes.signUpFormButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.signUpFormButton}
        >
            Войти
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.signUpFormButton}
        >
            Назад
        </button>
    </div>
);

export default SignUpFormButtons;
