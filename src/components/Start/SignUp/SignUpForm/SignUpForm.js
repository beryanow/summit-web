import React from 'react';
import { Formik, Form } from 'formik';

import SignUpFormField from './SignUpFormField/SignUpFormField';
import SignUpFormButtons from './SignUpFormButtons/SignUpFormButtons';

import classes from './SignUpForm.module.css';


const initialValues = {
    name: "",
    surname: "",
    email: "",
    username: "",
    password: "",
    age: ""
};

const validate = values => {
    const errors = {};
    const valuesArr = Object.entries(values);

    for (const [key, value] of valuesArr) {
        if (!value) {
            errors[key] = "Не заполнено";
            continue;
        }

        if (key === "age" && isNaN(Number.parseInt(value))) {
            errors[key] = "Введите число";
        }
    }

    return errors;
}

const SignUpForm = ({
    isSubmitting,
    onSignUpSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={onSignUpSubmit}
    >
        {() => (
            <Form className={classes.signUpForm}>
                <span className={classes.signUpFormTitle}>Регистрация</span>
                <SignUpFormField
                    id="sign-up-name"
                    name="name"
                    label="Имя"
                />
                <SignUpFormField
                    id="sign-up-surname"
                    name="surname"
                    label="Фамилия"
                />
                <SignUpFormField
                    id="sign-up-email"
                    name="email"
                    label="Почтовый ящик"
                />
                <SignUpFormField
                    id="sign-up-username"
                    name="username"
                    label="Пользовательское имя"
                />
                <SignUpFormField
                    id="sign-up-password"
                    name="password"
                    label="Пароль"
                />
                <SignUpFormField
                    id="sign-up-age"
                    name="age"
                    label="Возвраст"
                />
                <SignUpFormButtons
                    isSubmitting={isSubmitting}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default SignUpForm;
