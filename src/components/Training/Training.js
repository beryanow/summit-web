import React from 'react';
import { Switch, Route } from 'react-router-dom';

import TrainingMainContainer from './TrainingMain/TrainingMainContainer';
import MyTrainingsMainContainer from './MyTrainingsMain/MyTrainingsMainContainer';
import TrainingCreateContainer from './TrainingCreate/TrainingCreateContainer';
import DefiniteTrainingContainer from './DefiniteTraining/DefiniteTrainingContainer';
import DefaultTrainingContainer from "./DefaultTraining/DefaultTrainingContainer";

const Training = (props) => {
    const searchTrainingById = (id) => {
        for (const trainingElement of trainingInfo.training) {
            if (trainingElement.id === Number.parseInt(id)) return trainingElement;
        }

        const placeholderTraining = {name: "", cycle: "", trainingSchedules: [], exercises: []};
        return placeholderTraining;
    }

    const {trainingInfo, authInfo} = props;
    const {getTraining, getAllExercise, getDefaultTraining, createTraining, updateTraining, deleteTraining, trainingChangeHandle} = props;

    const trainingMainProps = {trainingInfo, authInfo, getTraining, trainingChangeHandle};
    const defaultTrainingProps = {trainingInfo, authInfo, getDefaultTraining, createTraining, trainingChangeHandle};
    const myTrainingsMainProps = {trainingInfo, authInfo, getTraining, trainingChangeHandle};
    const trainingCreateProps = {trainingInfo, authInfo, getAllExercise, createTraining, trainingChangeHandle};
    const definiteTrainingProps = {trainingInfo, authInfo, getTraining, getAllExercise, updateTraining, deleteTraining, trainingChangeHandle};

    return (
        <Switch>
            <Route exact path="/training" render={() => <TrainingMainContainer {...trainingMainProps} />}/>
            <Route exact path="/training/myTrainings" render={() => <MyTrainingsMainContainer {...myTrainingsMainProps} />} />
            <Route exact path="/training/default" render={() => <DefaultTrainingContainer {...defaultTrainingProps} />} />
            <Route exact path="/training/create" render={() => <TrainingCreateContainer {...trainingCreateProps} />} />
            <Route exact path="/training/:id" render={(props) => <DefiniteTrainingContainer {...definiteTrainingProps} training={searchTrainingById(props.match.params.id)} />} />
        </Switch>
    );
}

export default Training;