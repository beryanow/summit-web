import React from 'react';
import classes from './DefaultTraining.module.css';


const DefaultTraining = ({innerComponent}) => (
    <div className={classes.defaultTraining}>
        {innerComponent}
    </div>
);

export default DefaultTraining;