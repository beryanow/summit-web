import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import DefaultTraining from './DefaultTraining'
import DefaultTrainingForm from "./DefaultTrainingForm/DefaultTrainingForm";

class DefaultTrainingContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            redirectToMain: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {trainingInfo} = this.props;
        const {
            dataChanged,
            dataDeleted,
            error
        } = trainingInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    createSuccessHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({isSubmitting: false});

        toast.success("Дефолтная тренировка создана!");
    }


    operationErrorHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при выполнении операции!");
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    onSubmit = values => {
        const {type} = values;

        const {authInfo} = this.props;
        const {token} = authInfo;

        this.props.getDefaultTraining(token, type);
        this.setState({ isSubmitting: true });
    }

    render() {
        const {
            isSubmitting,
            redirectToMain
        } = this.state;
        if (redirectToMain) return <Redirect push to="/training" />;

        const defaultTrainingProps = {
            isSubmitting,
            onSubmit: this.onSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const innerComponent = <DefaultTrainingForm {...defaultTrainingProps} />;

        return (
            <DefaultTraining innerComponent={innerComponent} />
        );
    }
}

export default DefaultTrainingContainer;