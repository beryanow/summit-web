import React from 'react';
import classes from './DefaultTrainingButtons.module.css';


const DefaultTrainingButtons = ({
isSubmitting,
redirectToMainHandle
                     }) => (
    <div className={classes.defaultTrainingButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.defaultTrainingButton}
        >
            Получить стандартную тренировку
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.defaultTrainingButton}
        >
            Назад
        </button>
    </div>
);

export default DefaultTrainingButtons;