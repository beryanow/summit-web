import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';

import DefaultTrainingButtons from "./DefaultTrainingButtons/DefaultTrainingButtons";
import classes from './DefaultTrainingForm.module.css';


const validate = (values) => {
    const errors = {};
    if (!values.type) errors.type = "Не заполнено";
    console.log(errors);
    return errors;
}

const DefaultTrainingForm = ({
    isSubmitting,
    onSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{type: ""}}
        validate={validate}
        onSubmit={onSubmit}
    >
        {() => (
            <Form className={classes.defaultTrainingForm}>
                <span className={classes.defaultTrainingFormTitle}>Стандартная тренировка</span>
                <div>
                    <span className={classes.defaultTrainingContent}>
                        Мы прдлагаем вам стандартные тренировки для тренажёрного зала и воркаут площадки, в которые входит комплекс упражнений на все группы мышц.
                    </span>
                </div>
                <div>
                    <Field
                        name="type"
                        component="select"
                        className={classes.defaultTrainingTypeSelect}
                    >
                        <option defaultValue={true}>Выберите тип тренировки</option>
                        <option value={`Зал`} label={`Зал`}/>
                        <option value={`Улица`} label={`Улица`}/>
                    </Field>
                    <ErrorMessage
                        name="type"
                        component="div"
                    />
                </div>
                <DefaultTrainingButtons
                    isSubmitting={isSubmitting}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default DefaultTrainingForm;