import React from 'react';
import {FaDumbbell} from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import TrainingBody from './TrainingBody/TrainingBody';

import classes from './TrainingMain.module.css';


const TrainingMain = ({training}) => {
    const headerText = "#тренировка";
    const headerIcon = <FaDumbbell />;
    const footerLink = "/training/create";

    return (
        <div className={classes.training}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <TrainingBody training={training} />
            <SectionFooter createLink={footerLink} />
        </div>
    );
}

export default TrainingMain;
