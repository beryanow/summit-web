import React from 'react';

import UpcomingTrainingsWidget from './UpcomingTrainingsWidget/UpcomingTrainingsWidget';
import classes from './DefiniteScreen.module.css';
import DefaultTrainingWidget from "./DefaultTrainingWidget/DefaultTrainingWidget";


const DefiniteScreen = (props) => {
    const {screenTraining} = props;
   // const widget = screenTraining.map(training =>
    return (
        <div className={classes.widgetScreen}>
            <UpcomingTrainingsWidget /*key={training.id}*/ trainings={props.screenTraining} />
            <DefaultTrainingWidget/>
        </div>
    );
}

export default DefiniteScreen;
