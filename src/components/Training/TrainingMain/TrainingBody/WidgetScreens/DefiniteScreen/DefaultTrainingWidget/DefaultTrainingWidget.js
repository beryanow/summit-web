import React from 'react';
import {Link} from 'react-router-dom';

import classes from './DefaultTrainingWidget.module.css';


const DefaultTrainingWidget = () => {


    // const widgetBackgroundStr = trainings.background ? `data:image/png;base64,${trainings.background}` : "";
    // const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = classes.widgetSize;

    const widgetLink = `/training/default`;

    return (
        <Link to={widgetLink} //style={widgetBackgroundStyle}
              className={`${classes.trainingWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.trainingWidgetTitle}>Программы тренировок</span>
            <div className={classes.trainingTitle}>
                Тренажёрный зал
                <div className={classes.trainingWidgetContent}>
                    Возьмите максимум из тренировок<br/> в тренажёрном зале
                </div>
            </div>
            <div className={classes.trainingTitle}>
                Воркаут площадка
                <div className={classes.trainingWidgetContent}>
                    Тренируйся с собственным весом
                </div>
            </div>
        </Link>
    );
}

export default DefaultTrainingWidget;
