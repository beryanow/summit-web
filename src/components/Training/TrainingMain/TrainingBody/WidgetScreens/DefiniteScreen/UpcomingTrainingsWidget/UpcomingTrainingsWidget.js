import React from 'react';
import {Link} from 'react-router-dom';

import classes from './UpcomingTrainingsWidget.module.css';


const UpcomingTrainingsWidget = ({trainings}) => {
    const createExercises = exercises => {
        return exercises.map(exercise => {
            const text = `${exercise.name} x${exercise.repetitions}`;
            return <span key={exercise.id} className={classes.widgetExercises}>{text}</span>;
        });
    }


   // const widgetBackgroundStr = trainings.background ? `data:image/png;base64,${trainings.background}` : "";
   // const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = classes.widgetSize;

    const widgetLink = `/training/myTrainings`;

    return (
        <Link to={widgetLink} //style={widgetBackgroundStyle}
              className={`${classes.trainingWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.trainingWidgetTitle}>Ближайшие тренировки</span>
                {trainings.map((training) => (
                    <div className={classes.trainingTitle}>
                        {training.name}
                    <div className={classes.trainingWidgetContent}>
                        {createExercises(training.exercises)}
                    </div>
                    </div>
                ))}
        </Link>
    );
}

export default UpcomingTrainingsWidget;
