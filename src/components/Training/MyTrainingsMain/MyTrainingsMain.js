import React from 'react';
import {FaDumbbell} from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import MyTrainingsBody from './MyTrainingsBody/MyTrainingsBody';

import classes from './MyTrainingsMain.module.css';


const MyTrainingsMain = ({training}) => {
    const headerText = "#тренировка";
    const headerIcon = <FaDumbbell />;
    const footerLink = "/training/create";

    return (
        <div className={classes.training}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <MyTrainingsBody training={training} />
            <SectionFooter createLink={footerLink} />
        </div>
    );
}

export default MyTrainingsMain;
