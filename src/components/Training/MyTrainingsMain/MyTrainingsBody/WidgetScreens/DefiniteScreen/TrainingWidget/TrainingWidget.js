import React from 'react';
import { Link } from 'react-router-dom';

import classes from './TrainingWidget.module.css';


const TrainingWidget = ({training}) => {
    const createExercises = exercises => {
        return exercises.map(exercise => {
            const text = `${exercise.name} x${exercise.repetitions}`;
            return <span key={exercise.id} className={classes.widgetExercises}>{text}</span>;
        });
    }

    const exercises = createExercises(training.exercises);

    const widgetBackgroundStr = training.background ? `data:image/png;base64,${training.background}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = training.size === "2x2" ? classes.bigWidget : classes.middleWidget;

    const widgetLink = `/training/${training.id}`;

    return (
        <Link to={widgetLink} style={widgetBackgroundStyle} className={`${classes.trainingWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.trainingWidgetTitle}>{training.name}</span>
            <div className={classes.trainingWidgetContent}>
                {exercises}
            </div>
        </Link>
    );
}

export default TrainingWidget;
