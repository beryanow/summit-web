import React from 'react';

import TrainingWidget from './TrainingWidget/TrainingWidget';
import classes from './DefiniteScreen.module.css';


const DefiniteScreen = (props) => {
    const {screenTraining} = props;
    const widgets = screenTraining.map(training => <TrainingWidget key={training.id} training={training} />);

    return (
        <div className={classes.widgetScreen}>
            {widgets}
        </div>
    );
}

export default DefiniteScreen;
