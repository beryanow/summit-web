import React from 'react';

import WidgetScreens from './WidgetScreens';
import DefiniteScreen from './DefiniteScreen/DefiniteScreen';


class WidgetScreensContainer extends React.Component {
    createWidgetScreens = (training) => {
        const widgetScreensObj = {
            widgetScreensArr: [],
            currentTrainingIdx: 0
        }

        while (training.length > widgetScreensObj.currentTrainingIdx) {
            this.createNewDefiniteScreen(widgetScreensObj, training);
        }

        return widgetScreensObj.widgetScreensArr;
    }

    createNewDefiniteScreen = (widgetScreensObj, training) => {
        const screenObj = {
            screenTraining: [],
            freeCells: 8
        }

        while (screenObj.freeCells > 0 && training.length > widgetScreensObj.currentTrainingIdx) {
            const trainingElement = training[widgetScreensObj.currentTrainingIdx];
            screenObj.freeCells -= trainingElement.size === "2x2" ? 4 : 2;

            screenObj.screenTraining.push(trainingElement);

            widgetScreensObj.currentTrainingIdx += 1;
        }

        const definiteScreen = <DefiniteScreen key={widgetScreensObj.currentTrainingIdx} screenTraining={screenObj.screenTraining} />;
        widgetScreensObj.widgetScreensArr.push(definiteScreen);
    }


    render() {
        const {training} = this.props;
        const widgetScreensArr = this.createWidgetScreens(training);

        return (
            <WidgetScreens widgetScreens={widgetScreensArr} />
        );
    }
}

export default WidgetScreensContainer;