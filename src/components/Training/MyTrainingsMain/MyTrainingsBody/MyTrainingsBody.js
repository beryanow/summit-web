import React from 'react';
import {AiOutlineDoubleLeft, AiOutlineDoubleRight} from 'react-icons/all';

import WidgetScreensContainer from './WidgetScreens/WidgetScreensContainer';
import classes from './MyTrainingBodys.module.css';


const MyTrainingsBody = ({training}) => {
    const goLeft = () => {
        const element = document.getElementById("training-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft - element.clientWidth,
            behavior: "smooth"
        });
    }

    const goRight = () => {
        const element = document.getElementById("training-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft + element.clientWidth,
            behavior: "smooth"
        });
    }

    const isMoreOnePageCalc = (training) => {
        let summarySize = 0;

        for (const trainingElement of training) {
            summarySize += trainingElement.size === "2x2" ? 4 : 2;
            if (summarySize > 8) return true;
        }

        return false;
    }


    const isMoreOnePage = isMoreOnePageCalc(training);
    return (
        <div className={classes.trainingBody}>
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goLeft()}>
                    <AiOutlineDoubleLeft/>
                </div>
            }
            <WidgetScreensContainer training={training} />
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goRight()}>
                    <AiOutlineDoubleRight/>
                </div>
            }
        </div>
    );
}

export default MyTrainingsBody;
