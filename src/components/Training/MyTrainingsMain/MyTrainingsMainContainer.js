import React from 'react';
import { toast } from 'react-toastify';

import MyTrainingsMain from './MyTrainingsMain';


class MyTrainingsMainContainer extends React.Component {
    componentDidMount() {
        const {trainingInfo, authInfo} = this.props;
        const {backgrounds} = trainingInfo;
        const {token} = authInfo;

        this.props.getTraining(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {trainingInfo} = this.props;
        const {error} = trainingInfo;

        if (error) this.loadErrorHandle();
    }

    loadErrorHandle = () => {
        this.props.trainingChangeHandle();
        toast.error("Не удалось загрузить данные!");
    }

    render() {
        const {trainingInfo} = this.props;
        const {training} = trainingInfo;

        training.sort((first, second) => {
            const firstIdeaSize = first.size;
            const secondIdeaSize = second.size;

            if (firstIdeaSize > secondIdeaSize) return -1;
            else if (secondIdeaSize > firstIdeaSize) return 1;
            else return 0;

        });

        return (
            <MyTrainingsMain training={training} />
        );
    }
}

export default MyTrainingsMainContainer;
