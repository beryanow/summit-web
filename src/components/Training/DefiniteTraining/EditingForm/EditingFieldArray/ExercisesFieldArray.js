import React from 'react';
import { FieldArray } from 'formik';

import EditingField from './EditingField/EditingField';

import classes from './ExercisesFieldArray.module.css';


const ExercisesFieldArray = ({ isSubmitting, exercises, trainingId }) => (
    <FieldArray name="exercises" >
        {({ push, remove }) => (
            <div className={classes.exercisesFieldArray}>
                {
                    exercises.length > 0
                    &&
                    <span className={classes.exercisesFieldArrayTitle}>Упражнения</span>
                }
                {exercises.map((exercise, index) => (
                    <div key={index} className={classes.exercisesFieldArrayElement}>
                        <EditingField
                            id={`exercises-update-name-${index}`}
                            name={`exercises[${index}].name`}
                            label="Название"
                        />
                        <EditingField
                            id={`exercises-update-type-${index}`}
                            name={`exercises[${index}].type`}
                            label="Тип"
                        />
                        <EditingField
                            id={`exercises-update-description-${index}`}
                            name={`exercises[${index}].description`}
                            label="Описание"
                        />
                        <EditingField
                            id={`exercises-update-repetitions-${index}`}
                            name={`exercises[${index}].repetitions`}
                            label="Количество повторений"
                        />
                        <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                            Удалить
                        </button>
                    </div>
                ))}
                <button type="button" onClick={() => push({trainingId: trainingId, name: "", cycle: "", description: "", repetitions: ""})} disabled={isSubmitting}>
                    Добавить упражнение
                </button>
            </div>
        )}
    </FieldArray>
);

export default ExercisesFieldArray;
