import React from 'react';
import { FieldArray } from 'formik';

import EditingField from './EditingField/EditingField';

import classes from './TrainingScheduleFieldArray.module.css';


const TrainingScheduleFieldArray = ({ isSubmitting, trainingSchedules }) => (
    <FieldArray name="trainingSchedules" >
        {({ push, remove }) => (
            <div className={classes.trainingScheduleFieldArray}>
                {
                    trainingSchedules.length > 0
                    &&
                    <span className={classes.trainingScheduleFieldArrayTitle}>Расписание тренировок</span>
                }
                {trainingSchedules.map((date, index) => (
                    <div key={index} className={classes.trainingScheduleFieldArrayElement}>
                        <EditingField
                            id={`trainingSchedules-create-date-${index}`}
                            name={`trainingSchedules[${index}].date`}
                            label="Дата тренировки"
                        />
                        <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                            Удалить
                        </button>
                    </div>
                ))}
                <button type="button" onClick={() => push({date: ""})} disabled={isSubmitting}>
                    Добавить дату тренировки
                </button>
            </div>
        )}
    </FieldArray>
);

export default TrainingScheduleFieldArray;
