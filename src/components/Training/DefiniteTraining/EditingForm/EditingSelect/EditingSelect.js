import React from 'react';
import {FieldArray} from 'formik';
import {Field} from "formik";
// import EditingField from './EditingField/EditingField';
import classes from './EditingSelect.module.css';


const EditingSelect = ({isSubmitting, exercises}) => {
    return (
        <FieldArray name="exercises">
            {({push, remove}) => (
                <div className={classes.editingSelect}>
                    {
                        exercises.length > 0
                        &&
                        <span className={classes.editingSelectTitle}>Упражнения</span>
                    }
                    {exercises.map((exercise, index) => (
                        <div key={index} className={classes.editingSelectElement}>
                            <Field>

                            </Field>
                            <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                                Удалить
                            </button>
                        </div>
                    ))}
                    <button type="button" onClick={() => push(`{"name": "", "description": "", "repetitions": "", "type": ""}`)} disabled={isSubmitting}>
                        Добавить упражнение
                    </button>
                </div>

            )}
        </FieldArray>
    );

};

export default EditingSelect;