import React from 'react';
import {Formik, Form, Field} from 'formik';

import EditingField from "./EditingField/EditingField";
import ExercisesFieldArray from "./EditingFieldArray/ExercisesFieldArray";
import TrainingSchedulesFieldArray from "./EditingFieldArray/TrainingScheduleFieldArray";
import EditingButtons from "./EditingButtons/EditingButtons";

import classes from './EditingForm.module.css';

const validate = (values) => {
    const validateExercisesArr = exercises => {
        const exercisesError = [];
        let isErrorsExist = false;

        exercises.forEach((exercise) => {
            // const exerciseJSON = JSON.parse(exercise);

            const {name, description, type, repetitions} = exercise;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!description) errorObject.description = "Не заполнено";
            if (!type) errorObject.type = "Не заполнено";
            if (!repetitions) errorObject.repetition = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            exercisesError.push(errorObject);
        });

        return isErrorsExist ? exercisesError : null;
    }

    const validateTrainingScheduleArr = trainingSchedules => {
        const trainingScheduleError = [];
        let isErrorsExist = false;

        trainingSchedules.forEach((trainingDate) => {
            const {date} = trainingDate;
            const errorObject = {};

            if (!date) errorObject.date = "Не заполнено";


            isErrorsExist = Object.keys(errorObject).length !== 0;
            trainingScheduleError.push(errorObject);
        });

        return isErrorsExist ? trainingScheduleError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);
    for (const [key, value] of valuesArr) {
        if (key === "exercises") {
            const exercisesError = validateExercisesArr(value);
            if (exercisesError) errors.exercises = exercisesError;
            continue;
        }
        if (key === "trainingSchedules") {
            const trainingScheduleError = validateTrainingScheduleArr(value);
            if (trainingScheduleError) errors.trainingSchedules = trainingScheduleError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const EditingForm = ({
    isSubmitting,
    trainingId,
    initialValues,
    onUpdateSubmit,
    cancelHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={onUpdateSubmit}
    >
        {({values}) => (
            <Form className={classes.editingForm}>
                <span className={classes.editingFormTitle}>Изменение тренировки</span>
                <EditingField
                    id="training-create-name"
                    name="name"
                    label="Название тренировки"
                />
                <div>
                    <label style={{display: 'block'}} className={classes.editingFieldLabel}>
                        Уровень сложности
                    </label>
                    <Field
                        name="cycle"
                        component="select"
                        className={classes.editingCycleSelect}
                    >
                        <option defaultValue={true}>Выберите сложность</option>
                        <option value={`легкий`} label={`Легко`}/>
                        <option value={`средний`} label={`Средне`}/>
                        <option value={`сложный`} label={`Сложно`}/>
                        <option value={`персональный`} label={`Персональная`}/>
                    </Field>
                </div>
                <ExercisesFieldArray
                    isSubmitting={isSubmitting}
                    exercises={values.exercises}
                    trainingId={trainingId}
                />
                <TrainingSchedulesFieldArray
                    isSubmitting={isSubmitting}
                    trainingSchedules={values.trainingSchedules}
                />
                <EditingButtons
                    isSubmitting={isSubmitting}
                    cancelHandle={cancelHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default EditingForm;
