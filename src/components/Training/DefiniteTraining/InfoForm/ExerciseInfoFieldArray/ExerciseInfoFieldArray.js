import React from 'react';
import classes from './ExerciseInfoFieldArray.module.css';


const ExerciseInfoFieldArray = ({exercises}) => (
    <div className={classes.infoFieldArray}>
        {
            exercises.length > 0
            &&
            <span className={classes.infoFieldArrayTitle}>Упражнения</span>
        }
        {exercises.map((exercise, index) => (
            <div key={index} className={classes.infoFieldArrayElement}>
                {exercise.name}<br/>
                Описание: {exercise.description}<br/>
                Количество повторений: {exercise.repetitions}<br/>
                Тип: {exercise.type}
            </div>
        ))}
    </div>
);

export default ExerciseInfoFieldArray;