import React from 'react';
import classes from './InfoField.module.css';


const InfoField = ({value}) => (
    <div className={classes.infoField}>
        {value}
    </div>
);

export default InfoField;