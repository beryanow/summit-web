import React from 'react';
import classes from './TrainingScheduleInfoFieldArray.module.css';


const TrainingScheduleInfoFieldArray = ({trainingSchedule}) => (
    <div className={classes.infoFieldArray}>
        {
            trainingSchedule.length > 0
            &&
            <span className={classes.infoFieldArrayTitle}>Расписание тренировок</span>
        }
        {trainingSchedule.map((trainingSchedule, index) => (
            <div key={index} className={classes.infoFieldArrayElement}>
                Дата: {trainingSchedule.date}
            </div>
        ))}
    </div>
);

export default TrainingScheduleInfoFieldArray;