import React from 'react';
import { Formik, Form } from 'formik';

import InfoField from './InfoField/InfoField';
import ExerciseInfoFieldArray from './ExerciseInfoFieldArray/ExerciseInfoFieldArray';
import InfoButtons from './InfoButtons/InfoButtons';
import TrainingScheduleInfoFieldArray from "./TrainingScheduleInfoFieldArray/TrainingScheduleInfoFieldArray";
import classes from './InfoForm.module.css';


const InfoForm = ({
    isSubmitting,
    name,
    cycle,
    exercises,
    trainingSchedules,
    onDeleteSubmit,
    editingHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{}}
        onSubmit={onDeleteSubmit}
    >
        {() => (
            <Form className={classes.infoForm}>
                <span className={classes.infoFormTitle}>Тренировка</span>
                <InfoField value={name} />
                <InfoField value={cycle} />
                <ExerciseInfoFieldArray exercises={exercises} />
                <TrainingScheduleInfoFieldArray trainingSchedule={trainingSchedules} />
                <InfoButtons
                    isSubmitting={isSubmitting}
                    editingHandle={editingHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default InfoForm;