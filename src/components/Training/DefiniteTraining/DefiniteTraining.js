import React from 'react';
import classes from './DefiniteTraining.module.css';


const DefiniteTraining = ({innerComponent}) => (
    <div className={classes.definiteTraining}>
        {innerComponent}
    </div>
);

export default DefiniteTraining;