import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import InfoForm from './InfoForm/InfoForm';
import EditingForm from './EditingForm/EditingForm';
import DefiniteTraining from "./DefiniteTraining";


class DefiniteTrainingContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            isEditing: false,
            deleted: false,
            redirectToMain: false
        };
    }

    componentDidMount() {
        const {trainingInfo, authInfo} = this.props;
        if (trainingInfo.training.length === 0) {
            const {backgrounds} = trainingInfo;
            const {token} = authInfo;

            this.props.getTraining(backgrounds, token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {trainingInfo} = this.props;
        const {
            dataChanged,
            dataDeleted,
            error
        } = trainingInfo;

        if (dataChanged) this.updateSuccessHandle();
        else if (dataDeleted) this.deleteSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    updateSuccessHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({
            isSubmitting: false,
            isEditing: false
        });

        toast.success("Тренировка успешно обновлено!");
    }

    deleteSuccessHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({
            isSubmitting: false,
            deleted: true
        });

        toast.success("Тренировка удалено успешно!");
    }

    operationErrorHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при выполнении операции!");
    }

    onUpdateSubmit = values => {
        const {name, cycle, exercises, trainingSchedules} = values;

        const {training, authInfo} = this.props;
        const {id, backgroundId} = training;
        const {userId, token} = authInfo;

        const exercisesForFetch = exercises.map(exercise => ({...exercise, trainingId: training.id}));
        const trainingScheduleForFetch = trainingSchedules.map(date => ({...date}))

        const updatedTraining = {
            id,
            name,
            cycle,
            exercises: exercisesForFetch,
            trainingSchedules: trainingScheduleForFetch,
            backgroundId,
            userId
        };

        this.props.updateTraining(updatedTraining, token);
        this.setState({isSubmitting: true});
    }

    onDeleteSubmit = () => {
        const {training, authInfo} = this.props;
        const {id} = training;
        const {token} = authInfo;

        this.props.deleteTraining(id, token);
        this.setState({isSubmitting: true});
    }

    editingHandle = () => {
        const {isEditing} = this.state;
        this.setState({isEditing: !isEditing});
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {
            isSubmitting,
            isEditing,
            deleted,
            redirectToMain
        } = this.state;
        if (deleted || redirectToMain) return <Redirect push to="/training" />;

        const {training} = this.props;
        const {name, cycle, exercises, trainingSchedules} = training;

        const editingProps = {
            isSubmitting,
            trainingId: this.props.training.id,
            initialValues: {name, cycle, exercises, trainingSchedules},
            onUpdateSubmit: this.onUpdateSubmit,
            cancelHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const infoProps = {
            isSubmitting,
            name,
            cycle,
            exercises,
            trainingSchedules,
            onDeleteSubmit: this.onDeleteSubmit,
            editingHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const innerComponent = isEditing ? <EditingForm {...editingProps} /> : <InfoForm {...infoProps} />;

        return (
            <DefiniteTraining innerComponent={innerComponent} />
        );
    }
}

export default DefiniteTrainingContainer;