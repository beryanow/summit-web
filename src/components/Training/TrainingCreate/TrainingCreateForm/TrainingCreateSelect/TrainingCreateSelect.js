import React from 'react';
import {FieldArray} from 'formik';
import {Field} from "formik";
// import EditingField from './EditingField/EditingField';
import classes from './TrainingCreateSelect.module.css';


const TrainingCreateSelect = ({isSubmitting, exercisesAll, exercises}) => {
    return (
        <FieldArray name="exercises">
            {({push, remove}) => (
                <div className={classes.trainingCreateSelect}>
                    {
                        exercises.length > 0
                        &&
                        <span className={classes.trainingCreateSelectTitle}>Упражнения</span>
                    }
                    {exercises.map((exercise, index) => (
                        <div key={index} className={classes.trainingCreateSelectElement}>
                            <Field
                                name={`exercises[${index}]`}
                                component="select"
                                className={classes.trainingCreateExerciseSelect}
                            >
                                <option defaultValue={true}>Выберите упражение</option>
                                {exercisesAll.map((exercise, index) => (
                                        <option value={`{"name": "${exercise.name}", "description": "${exercise.description}", "repetitions": "${exercise.repetitions}", "type": "${exercise.type}"}`} label={`${exercisesAll[index].name} x${exercisesAll[index].repetitions}`}/>
                                    )
                                )}
                            </Field>
                            <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                                Удалить
                            </button>
                        </div>
                    ))}
                    <button type="button" onClick={() => push(`{"name": "", "description": "", "repetitions": "", "type": ""}`)} disabled={isSubmitting}>
                        Добавить упражнение
                    </button>
                </div>

            )}
        </FieldArray>
    );

};

export default TrainingCreateSelect;