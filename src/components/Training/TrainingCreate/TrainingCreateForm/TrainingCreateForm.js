import React from 'react';
import {Formik, Form, Field} from 'formik';

import TrainingCreateField from './TrainingCreateField/TrainingCreateField';
import TrainingCreateButtons from './TrainingCreateButtons/TrainingCreateButtons';
import TrainingCreateSelect from "./TrainingCreateSelect/TrainingCreateSelect";

import classes from './TrainingCreateForm.module.css';


const validate = (values) => {
    const validateExercisesArr = exercises => {
        const exercisesError = [];
        let isErrorsExist = false;

        exercises.forEach((exercise) => {
            const exerciseJSON = JSON.parse(exercise);

            const {name, description, type, repetitions} = exerciseJSON;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!description) errorObject.description = "Не заполнено";
            if (!type) errorObject.type = "Не заполнено";
            if (!repetitions) errorObject.repetition = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            exercisesError.push(errorObject);
        });

        return isErrorsExist ? exercisesError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);
    for (const [key, value] of valuesArr) {
        if (key === "exercises") {
            const exercisesError = validateExercisesArr(value);
            if (exercisesError) errors.exercises = exercisesError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const TrainingCreateForm = ({
    isSubmitting,
    exercisesAll,
    onCreateSubmit,
    redirectToMainHandle
}) => {
    return (
        <Formik
            initialValues={{
                name: "",
                cycle: "",
                exercises: []
            }}
            validate={validate}
            onSubmit={onCreateSubmit}
        >
            {({values}) => (
                <Form className={classes.trainingCreateForm}>
                    <span className={classes.trainingCreateFormTitle}>Создание тренировки</span>
                    <TrainingCreateField
                        id="training-create-name"
                        name="name"
                        label="Название тренировки"
                    />
                    <div>
                        <label style={{display: 'block'}} className={classes.trainingCreateFieldLabel}>
                            Уровень сложности
                        </label>
                        <Field
                            name="cycle"
                            component="select"
                            className={classes.trainingCreateSelect}
                        >
                            <option defaultValue={true}>Выберите сложность</option>
                            <option value={`легкий`} label={`Легко`}/>
                            <option value={`средний`} label={`Средне`}/>
                            <option value={`сложный`} label={`Сложно`}/>
                            <option value={`персональный`} label={`Персональная`}/>
                        </Field>
                    </div>
                    <TrainingCreateSelect
                        isSubmitting={isSubmitting}
                        exercisesAll={exercisesAll}
                        exercises={values.exercises}
                    />
                    <TrainingCreateButtons
                        isSubmitting={isSubmitting}
                        redirectToMainHandle={redirectToMainHandle}
                    />
                </Form>
            )}
        </Formik>
    );
};

export default TrainingCreateForm;
