import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './TrainingCreateField.module.css';


const TrainingCreateField = ({ id, name, label }) => (
    <div className={classes.trainingCreateField}>
        <label
            htmlFor={id}
            className={classes.trainingCreateFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.trainingCreateFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.trainingCreateFieldError}
        />
    </div>
);

export default TrainingCreateField;