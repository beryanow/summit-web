import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './TrainingScheduleField.module.css';


const TrainingScheduleField = ({ id, name, label }) => (
    <div className={classes.trainingScheduleField}>
        <label
            htmlFor={id}
            className={classes.trainingScheduleFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.trainingScheduleFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.trainingScheduleFieldError}
        />
    </div>
);

export default TrainingScheduleField;