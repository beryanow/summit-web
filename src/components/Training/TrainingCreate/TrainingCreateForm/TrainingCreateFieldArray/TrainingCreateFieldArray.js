import React from 'react';
import { FieldArray } from 'formik';

import TrainingScheduleField from './TrainingScheduleField/TrainingScheduleField';
import classes from './TrainingCreateFieldArray.module.css';


const TrainingCreateFieldArray = ({ isSubmitting, trainingSchedule }) => (
    <FieldArray name="trainingSchedule" >
        {({ push, remove }) => (
            <div className={classes.trainingCreateFieldArray}>
                {
                    trainingSchedule.length > 0
                    &&
                    <span className={classes.trainingCreateFieldArrayTitle}>Расписание тренировок</span>
                }
                {trainingSchedule.map((exercise, index) => (
                    <div key={index} className={classes.trainingCreateFieldArrayElement}>
                        <TrainingScheduleField
                            id={`trainingSchedule-create-date-${index}`}
                            name={`trainingSchedule[${index}].date`}
                            label="Дата тренировки"
                        />
                        <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                            Удалить
                        </button>
                    </div>
                ))}
                <button type="button" onClick={() => push({date: ""})} disabled={isSubmitting}>
                    Добавить дату тренировки
                </button>
            </div>
        )}
    </FieldArray>
);

export default TrainingCreateFieldArray;