import React from 'react';
import classes from './TrainingCreateButtons.module.css';


const TrainingCreateButtons = ({ isSubmitting, redirectToMainHandle }) => (
    <div className={classes.trainingCreateButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.trainingCreateButton}
        >
            Создать тренировку
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.trainingCreateButton}
        >
            Назад
        </button>
    </div>
);

export default TrainingCreateButtons;