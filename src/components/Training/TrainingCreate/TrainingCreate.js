import React from 'react';

import TrainingCreateForm from './TrainingCreateForm/TrainingCreateForm';
import classes from './TrainingCreate.module.css';


const TrainingCreate = (props) => {
    return (
        <div className={classes.trainingCreate}>
            <TrainingCreateForm {...props} />
        </div>
    );
};

export default TrainingCreate;