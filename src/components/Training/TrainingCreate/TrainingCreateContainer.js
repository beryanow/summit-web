import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import TrainingCreate from './TrainingCreate';


class TrainingCreateContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSubmitting: false,
            redirectToMain: false
        }
    }

    componentDidMount() {
        const {authInfo} = this.props;
        const {token} = authInfo;

        this.props.getAllExercise(token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {trainingInfo} = this.props;
        const {dataChanged, error} = trainingInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.createErrorHandle();
    }

    createSuccessHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({isSubmitting: false});

        toast.success("Тренировка успешно создана!");
    }

    createErrorHandle = () => {
        this.props.trainingChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при создании тренировки!");
    }

    onCreateSubmit = values => {
        const {name, cycle, exercises} = values;

        const {authInfo} = this.props;
        const {userId, token} = authInfo;

        const exercisesForFetch = exercises.map(exercise => JSON.parse(exercise));
        // const trainingScheduleForFetch = trainingSchedule.map(trainingSchedule => ({...trainingSchedule, userId}));

        const createdTraining = {
            name,
            cycle,
            exercises: exercisesForFetch,
            userId
        };

    // debugger;
        // ДО СЮДА РАБОТАЕТ
        this.props.createTraining(createdTraining, token);
        this.setState({ isSubmitting: true });
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {isSubmitting, redirectToMain} = this.state;
        if (redirectToMain) return <Redirect push to="/training" />;

        const { exercises } = this.props.trainingInfo;

        const trainingCreateProps = {
            isSubmitting,
            exercisesAll: exercises,
            onCreateSubmit: this.onCreateSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        }
        return <TrainingCreate {...trainingCreateProps} />;
    }
}

export default TrainingCreateContainer;