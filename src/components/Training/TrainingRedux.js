import { connect } from 'react-redux';

import Training from './Training';
import {
    getTrainingFetch,
    getAllExerciseFetch,
    createTrainingFetch,
    updateTrainingFetch,
    deleteTrainingFetch,
    trainingChangeHandle, getDefaultTrainingFetch
} from '../../actions/trainingActionCreator';


const mapStateToProps = ({trainingInfo, authInfo}) => ({
    trainingInfo,
    authInfo
})

const mapDispatchToProps = dispatch => ({
    getTraining: (backgrounds, token) => dispatch(getTrainingFetch(backgrounds, token)),
    getAllExercise: (token) => dispatch(getAllExerciseFetch(token)),
    getDefaultTraining: (token, type) => dispatch(getDefaultTrainingFetch(token, type)),
    createTraining: (training, token) => dispatch(createTrainingFetch(training, token)),
    updateTraining: (training, token) => dispatch(updateTrainingFetch(training, token)),
    deleteTraining: (id, token) => dispatch(deleteTrainingFetch(id, token)),
    trainingChangeHandle: () => dispatch(trainingChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Training);