import React from "react";
import { Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import StartRedux from '../Start/StartRedux';
import GoalsRedux from '../Goals/GoalsRedux';
import IdeasRedux from '../Ideas/IdeasRedux';
import NutritionRedux from '../Nutrition/NutritionRedux';
import TrainingRedux from '../Training/TrainingRedux';
import ProfileRedux from '../Profile/ProfileRedux';

import WithAuthHOC from '../Start/WithAuthHOC/WithAuthHOC';


function App() {
  return (
      <>
          <ToastContainer />
          <Switch>
              <Route path="/goals" component={WithAuthHOC(GoalsRedux)} />
              <Route path="/ideas" component={WithAuthHOC(IdeasRedux)} />
              <Route path="/nutrition" component={WithAuthHOC(NutritionRedux)} />
              <Route path="/profile" component={WithAuthHOC(ProfileRedux)} />
              <Route path="/training" component={TrainingRedux} />
              <Route path="/" component={StartRedux} />
          </Switch>
      </>
  );
}

export default App;
