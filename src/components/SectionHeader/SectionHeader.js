import React from "react";
import classes from "./SectionHeader.module.css";


const SectionHeader = ({text, icon}) => {
    return (
        <div className={classes.sectionHeader}>
            <div className={classes.logo}>
                {icon}
                {text}
            </div>
        </div>
    );
}

export default SectionHeader;