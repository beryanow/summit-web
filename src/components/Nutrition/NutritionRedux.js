import { connect } from 'react-redux';

import Nutrition from './Nutrition';
import {
    getNutritionFetch,
    createNutritionFetch,
    updateNutritionFetch,
    deleteNutritionFetch,
    nutritionChangeHandle
} from '../../actions/nutritionActionCreator';


const mapStateToProps = ({nutritionInfo, backgroundsInfo, authInfo}) => ({
    nutritionInfo,
    backgroundsInfo,
    authInfo
})

const mapDispatchToProps = dispatch => ({
    getNutrition: (backgrounds, token) => dispatch(getNutritionFetch(backgrounds, token)),
    createNutrition: (nutrition, token) => dispatch(createNutritionFetch(nutrition, token)),
    updateNutrition: (nutrition, token) => dispatch(updateNutritionFetch(nutrition, token)),
    deleteNutrition: (id, token) => dispatch(deleteNutritionFetch(id, token)),
    nutritionChangeHandle: () => dispatch(nutritionChangeHandle())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Nutrition);