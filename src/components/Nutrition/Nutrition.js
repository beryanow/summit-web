import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NutritionMainContainer from './NutritionMain/NutritionMainContainer';
import NutritionCreateContainer from './NutritionCreate/NutritionCreateContainer';
import DefiniteNutritionContainer from './DefiniteNutrition/DefiniteNutritionContainer';


const Nutrition = (props) => {
    const searchNutritionById = (id) => {
        for (const nutritionElement of nutritionInfo.nutrition) {
            if (nutritionElement.id === Number.parseInt(id)) return nutritionElement;
        }

        const placeholderNutrition = {name: "", description: "", recipe: "", ingredients: []};
        return placeholderNutrition;
    }

    const {nutritionInfo, backgroundsInfo, authInfo} = props;
    const {getNutrition, createNutrition, updateNutrition, deleteNutrition, nutritionChangeHandle} = props;

    const nutritionMainProps = {nutritionInfo, backgroundsInfo, authInfo, getNutrition, nutritionChangeHandle};
    const nutritionCreateProps = {nutritionInfo, authInfo, createNutrition, nutritionChangeHandle};
    const definiteNutritionProps = {nutritionInfo, backgroundsInfo, authInfo, getNutrition, updateNutrition, deleteNutrition, nutritionChangeHandle};

    return (
        <Switch>
            <Route exact path="/nutrition" render={() => <NutritionMainContainer {...nutritionMainProps} />} />
            <Route exact path="/nutrition/create" render={() => <NutritionCreateContainer {...nutritionCreateProps} />} />
            <Route exact path="/nutrition/:id" render={(props) => <DefiniteNutritionContainer {...definiteNutritionProps} nutrition={searchNutritionById(props.match.params.id)} />} />
        </Switch>
    );
}

export default Nutrition;