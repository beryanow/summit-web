import React from 'react';
import { GiKnifeFork } from 'react-icons/all';

import SectionHeader from '../../SectionHeader/SectionHeader';
import SectionFooter from '../../SectionFooter/SectionFooter';
import NutritionBody from './NutritionBody/NutritionBody';

import classes from './NutritionMain.module.css';


const NutritionMain = ({nutrition}) => {
    const headerText = "#питание";
    const headerIcon = <GiKnifeFork />;
    const footerLink = "/nutrition/create";

    return (
        <div className={classes.nutrition}>
            <SectionHeader text={headerText} icon={headerIcon} />
            <NutritionBody nutrition={nutrition} />
            <SectionFooter createLink={footerLink} />
        </div>
    );
}

export default NutritionMain;
