import React from 'react';
import classes from './WidgetScreens.module.css';


const WidgetScreens = (props) => {
    const {widgetScreens} = props;

    return (
        <div id="nutrition-widgets-screens" className={classes.widgetScreens}>
            {widgetScreens}
        </div>
    );
}

export default WidgetScreens;
