import React from 'react';

import NutritionWidget from './NutritionWidget/NutritionWidget';
import classes from './DefiniteScreen.module.css';


const DefiniteScreen = (props) => {
    const {screenNutrition} = props;
    const widgets = screenNutrition.map(nutrition => <NutritionWidget key={nutrition.id} nutrition={nutrition} />);

    return (
        <div className={classes.widgetScreen}>
            {widgets}
        </div>
    );
}

export default DefiniteScreen;
