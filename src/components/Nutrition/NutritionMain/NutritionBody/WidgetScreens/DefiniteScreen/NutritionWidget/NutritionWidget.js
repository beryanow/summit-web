import React from 'react';
import { Link } from 'react-router-dom';

import classes from './NutritionWidget.module.css';


const NutritionWidget = ({nutrition}) => {
    const createIngredients = ingredients => {
        return ingredients.map(ingredient => {
            const text = `${ingredient.name} - ${ingredient.amount}`;
            return <span key={ingredient.id} className={classes.widgetIngredient}>{text}</span>;
        });
    }

    const ingredients = createIngredients(nutrition.ingredients);

    const widgetBackgroundStr = nutrition.background ? `data:image/png;base64,${nutrition.background}`: "";
    const widgetBackgroundStyle = widgetBackgroundStr ? {backgroundImage: `url(${widgetBackgroundStr})`} : {};

    const widgetSizeCSSClass = nutrition.size === "2x2" ? classes.bigWidget : classes.middleWidget;

    const widgetLink = `/nutrition/${nutrition.id}`;

    return (
        <Link to={widgetLink} style={widgetBackgroundStyle} className={`${classes.nutritionWidget} ${widgetSizeCSSClass}`}>
            <span className={classes.nutritionWidgetTitle}>{nutrition.name}</span>
            <div className={classes.nutritionWidgetContent}>
                {ingredients}
            </div>
        </Link>
    );
}

export default NutritionWidget;
