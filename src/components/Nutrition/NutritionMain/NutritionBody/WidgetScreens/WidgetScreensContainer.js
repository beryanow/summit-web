import React from 'react';

import WidgetScreens from './WidgetScreens';
import DefiniteScreen from './DefiniteScreen/DefiniteScreen';


class WidgetScreensContainer extends React.Component {
    createWidgetScreens = (nutrition) => {
        const widgetScreensObj = {
            widgetScreensArr: [],
            currentNutritionIdx: 0
        }

        while (nutrition.length > widgetScreensObj.currentNutritionIdx) {
            this.createNewDefiniteScreen(widgetScreensObj, nutrition);
        }

        return widgetScreensObj.widgetScreensArr;
    }

    createNewDefiniteScreen = (widgetScreensObj, nutrition) => {
        const screenObj = {
            screenNutrition: [],
            freeCells: 16
        }

        while (screenObj.freeCells > 0 && nutrition.length > widgetScreensObj.currentNutritionIdx) {
            const nutritionElement = nutrition[widgetScreensObj.currentNutritionIdx];
            screenObj.freeCells -= nutritionElement.size === "2x2" ? 4 : 2;

            screenObj.screenNutrition.push(nutritionElement);

            widgetScreensObj.currentNutritionIdx += 1;
        }

        const definiteScreen = <DefiniteScreen key={widgetScreensObj.currentNutritionIdx} screenNutrition={screenObj.screenNutrition} />;
        widgetScreensObj.widgetScreensArr.push(definiteScreen);
    }


    render() {
        const {nutrition} = this.props;
        const widgetScreensArr = this.createWidgetScreens(nutrition);

        return (
            <WidgetScreens widgetScreens={widgetScreensArr} />
        );
    }
}

export default WidgetScreensContainer;