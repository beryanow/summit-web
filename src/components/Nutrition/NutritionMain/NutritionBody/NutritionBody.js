import React from 'react';
import {AiOutlineDoubleLeft, AiOutlineDoubleRight} from 'react-icons/all';

import WidgetScreensContainer from './WidgetScreens/WidgetScreensContainer';
import classes from './NutritionBody.module.css';


const NutritionBody = ({nutrition}) => {
    const goLeft = () => {
        const element = document.getElementById("nutrition-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft - element.clientWidth,
            behavior: "smooth"
        });
    }

    const goRight = () => {
        const element = document.getElementById("nutrition-widgets-screens");

        element.scrollTo({
            top: 0,
            left: element.scrollLeft + element.clientWidth,
            behavior: "smooth"
        });
    }

    const isMoreOnePageCalc = (nutrition) => {
        let summarySize = 0;

        for (const nutritionElement of nutrition) {
            summarySize += nutritionElement.size === "2x2" ? 4 : 2;
            if (summarySize > 16) return true;
        }

        return false;
    }


    const isMoreOnePage = isMoreOnePageCalc(nutrition);
    return (
        <div className={classes.nutritionBody}>
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goLeft()}>
                    <AiOutlineDoubleLeft/>
                </div>
            }
            <WidgetScreensContainer nutrition={nutrition} />
            {
                isMoreOnePage &&
                <div className={classes.screensControl} onClick={() => goRight()}>
                    <AiOutlineDoubleRight/>
                </div>
            }
        </div>
    );
}

export default NutritionBody;
