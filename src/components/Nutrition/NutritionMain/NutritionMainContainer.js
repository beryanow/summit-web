import React from 'react';
import { toast } from 'react-toastify';

import NutritionMain from './NutritionMain';


class NutritionMainContainer extends React.Component {
    componentDidMount() {
        const {backgroundsInfo, authInfo} = this.props;
        const {backgrounds} = backgroundsInfo;
        const {token} = authInfo;

        this.props.getNutrition(backgrounds, token);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {nutritionInfo} = this.props;
        const {error} = nutritionInfo;

        if (error) this.loadErrorHandle();
    }

    loadErrorHandle = () => {
        this.props.nutritionChangeHandle();
        toast.error("Не удалось загрузить данные!");
    }

    render() {
        const {nutritionInfo} = this.props;
        const {nutrition} = nutritionInfo;

        nutrition.sort((first, second) => {
            const firstIdeaSize = first.size;
            const secondIdeaSize = second.size;

            if (firstIdeaSize > secondIdeaSize) return -1;
            else if (secondIdeaSize > firstIdeaSize) return 1;
            else return 0;

        });

        return (
            <NutritionMain nutrition={nutrition} />
        );
    }
}

export default NutritionMainContainer;
