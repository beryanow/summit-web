import React from 'react';

import NutritionCreateForm from './NutritionCreateForm/NutritionCreateForm';
import classes from './NutritionCreate.module.css';


const NutritionCreate = (props) => (
    <div className={classes.nutritionCreate}>
        <NutritionCreateForm {...props} />
    </div>
);

export default NutritionCreate;