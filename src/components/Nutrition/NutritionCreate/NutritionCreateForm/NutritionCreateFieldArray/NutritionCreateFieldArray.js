import React from 'react';
import { FieldArray } from 'formik';

import IngredientField from './IngredientField/IngredientField';
import classes from './NutritionCreateFieldArray.module.css';


const NutritionCreateFieldArray = ({ isSubmitting, ingredients }) => (
    <FieldArray name="ingredients" >
        {({ push, remove }) => (
            <div className={classes.nutritionCreateFieldArray}>
                {
                    ingredients.length > 0
                    &&
                    <span className={classes.nutritionCreateFieldArrayTitle}>Ингредиенты</span>
                }
                {ingredients.map((ingredient, index) => (
                    <div key={index} className={classes.nutritionCreateFieldArrayElement}>
                        <IngredientField
                            id={`ingredients-create-name-${index}`}
                            name={`ingredients[${index}].name`}
                            label="Ингредиент"
                        />
                        <IngredientField
                            id={`ingredients-create-amount-${index}`}
                            name={`ingredients[${index}].amount`}
                            label="Количество"
                        />
                        <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                            Удалить
                        </button>
                    </div>
                ))}
                <button type="button" onClick={() => push({name: "", amount: ""})} disabled={isSubmitting}>
                    Добавить ингредиент
                </button>
            </div>
        )}
    </FieldArray>
);

export default NutritionCreateFieldArray;