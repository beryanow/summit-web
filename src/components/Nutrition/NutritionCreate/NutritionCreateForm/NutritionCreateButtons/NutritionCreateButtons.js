import React from 'react';
import classes from './NutritionCreateButtons.module.css';


const NutritionCreateButtons = ({ isSubmitting, redirectToMainHandle }) => (
    <div className={classes.nutritionCreateButtons}>
        <button
            type="submit"
            disabled={isSubmitting}
            className={classes.nutritionCreateButton}
        >
            Создать блюдо
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.nutritionCreateButton}
        >
            Назад
        </button>
    </div>
);

export default NutritionCreateButtons;