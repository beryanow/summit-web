import React from 'react';
import classes from './NutritionCreateImageUpload.module.css';


const NutritionCreateImageUpload = ({
    isSuccess,
    isError,
    errorMessage,
    onLoad
}) => {
    const additionalClassname = isSuccess ?
        classes.nutritionCreateImageUploadStatusSuccess :
        isError ? classes.nutritionCreateImageUploadStatusError : "";
    const statusMessage = isSuccess ? "Изображение успешно загружено" : errorMessage;

    return (
        <div className={classes.nutritionCreateImageUpload}>
            <label
                htmlFor="nutrition-create-upload"
                className={classes.nutritionCreateImageUploadLabel}
            >
                Загрузить изображение
            </label>
            <input
                id="nutrition-create-upload"
                type="file"
                onChange={onLoad}
                className={classes.nutritionCreateImageUploadInput}
            />
            <div className={`${classes.nutritionCreateImageUploadStatus} ${additionalClassname}`}>
                {statusMessage}
            </div>
        </div>
    );
}

export default NutritionCreateImageUpload;