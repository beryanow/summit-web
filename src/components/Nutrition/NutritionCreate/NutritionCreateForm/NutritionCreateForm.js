import React from 'react';
import { Formik, Form } from 'formik';

import NutritionCreateField from './NutritionCreateField/NutritionCreateField';
import NutritionCreateFieldArray from './NutritionCreateFieldArray/NutritionCreateFieldArray';
import NutritionCreateButtons from './NutritionCreateButtons/NutritionCreateButtons';
import NutritionCreateImageUploadContainer from './NutritionCreateImageUpload/NutritionCreateImageUploadContainer';

import classes from './NutritionCreateForm.module.css';


const validate = (values) => {
    const validateIngredientsArr = ingredients => {
        const ingredientsError = [];
        let isErrorsExist = false;

        ingredients.forEach((ingredient) => {
            const {name, amount} = ingredient;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!amount) errorObject.amount = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            ingredientsError.push(errorObject);
        });

        return isErrorsExist ? ingredientsError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);

    for (const [key, value] of valuesArr) {
        if (key === "ingredients") {
            const ingredientsError = validateIngredientsArr(value);
            if (ingredientsError) errors.ingredients = ingredientsError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const NutritionCreateForm = ({
    isSubmitting,
    setBackgroundBase64,
    onCreateSubmit,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={{
            name: "",
            description: "",
            recipe: "",
            ingredients: []
        }}
        validate={validate}
        onSubmit={onCreateSubmit}
    >
        {({values}) => (
            <Form className={classes.nutritionCreateForm}>
                <span className={classes.nutritionCreateFormTitle}>Создание блюда</span>
                <NutritionCreateField
                    id="nutrition-create-name"
                    name="name"
                    label="Название блюда"
                />
                <NutritionCreateField
                    id="nutrition-create-description"
                    name="description"
                    label="Описание блюда"
                />
                <NutritionCreateField
                    id="nutrition-create-recipe"
                    name="recipe"
                    label="Рецепт блюда"
                />
                <NutritionCreateImageUploadContainer
                    setBackgroundBase64={setBackgroundBase64}
                />
                <NutritionCreateFieldArray
                    isSubmitting={isSubmitting}
                    ingredients={values.ingredients}
                />
                <NutritionCreateButtons
                    isSubmitting={isSubmitting}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default NutritionCreateForm;
