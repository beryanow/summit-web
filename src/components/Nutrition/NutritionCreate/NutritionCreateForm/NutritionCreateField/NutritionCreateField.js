import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './NutritionCreateField.module.css';


const NutritionCreateField = ({ id, name, label }) => (
    <div className={classes.nutritionCreateField}>
        <label
            htmlFor={id}
            className={classes.nutritionCreateFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.nutritionCreateFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.nutritionCreateFieldError}
        />
    </div>
);

export default NutritionCreateField;