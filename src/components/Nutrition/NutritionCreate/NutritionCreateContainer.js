import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import NutritionCreate from './NutritionCreate';


class NutritionCreateContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            redirectToMain: false
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {nutritionInfo} = this.props;
        const {dataChanged, error} = nutritionInfo;

        if (dataChanged) this.createSuccessHandle();
        else if (error) this.createErrorHandle();
    }

    createSuccessHandle = () => {
        this.props.nutritionChangeHandle();
        this.setState({isSubmitting: false});

        toast.success("Рецепт успешно создан!");
    }

    createErrorHandle = () => {
        this.props.nutritionChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при создании вашего рецепта!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onCreateSubmit = values => {
        const {name, description, recipe, ingredients} = values;
        const {backgroundBase64} = this.state;

        const {authInfo} = this.props;
        const {userId, token} = authInfo;

        const ingredientsForFetch = ingredients.map(ingredients => ({...ingredients, userId}));
        const size = ingredients.length >= 2 ? "2x2" : "2x1";

        const createdNutrition = {
            name,
            description,
            recipe,
            ingredients: ingredientsForFetch,
            size,
            backgroundId: 1,
            background: backgroundBase64,
            userId
        };

        this.props.createNutrition(createdNutrition, token);
        this.setState({isSubmitting: true});
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {isSubmitting, redirectToMain} = this.state;
        if (redirectToMain) return <Redirect push to="/nutrition" />;

        const nutritionCreateProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            onCreateSubmit: this.onCreateSubmit,
            redirectToMainHandle: this.redirectToMainHandle
        }
        return <NutritionCreate {...nutritionCreateProps} />;
    }
}

export default NutritionCreateContainer;