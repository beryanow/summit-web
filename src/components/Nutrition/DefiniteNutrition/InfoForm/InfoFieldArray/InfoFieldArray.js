import React from 'react';
import classes from './InfoFieldArray.module.css';


const InfoFieldArray = ({ingredients}) => (
    <div className={classes.infoFieldArray}>
        {
            ingredients.length > 0
            &&
            <span className={classes.infoFieldArrayTitle}>Ингредиенты</span>
        }
        {ingredients.map((ingredient, index) => (
            <div key={index} className={classes.infoFieldArrayElement}>
                {ingredient.name} - {ingredient.amount}
            </div>
        ))}
    </div>
);

export default InfoFieldArray;