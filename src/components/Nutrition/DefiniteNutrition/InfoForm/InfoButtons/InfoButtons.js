import React from 'react';
import classes from './InfoButtons.module.css';


const InfoButtons = ({
    isSubmitting,
    editingHandle,
    redirectToMainHandle
}) => (
    <div className={classes.infoButtons}>
        <button
            type="button"
            onClick={editingHandle}
            disabled={isSubmitting}
            className={classes.infoButton}
        >
            Изменить
        </button>
        <button
            type="submit"
            disabled={isSubmitting}
            className={`${classes.infoButton} ${classes.deleteButton}`}
        >
            Удалить
        </button>
        <button
            type="button"
            onClick={redirectToMainHandle}
            disabled={isSubmitting}
            className={classes.infoButton}
        >
            Назад
        </button>
    </div>
);

export default InfoButtons;