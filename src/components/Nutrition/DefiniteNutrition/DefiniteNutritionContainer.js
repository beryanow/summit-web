import React from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import InfoForm from './InfoForm/InfoForm';
import EditingForm from './EditingForm/EditingForm';
import DefiniteNutrition from './DefiniteNutrition';


class DefiniteNutritionContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundBase64: "",
            isSubmitting: false,
            isEditing: false,
            deleted: false,
            redirectToMain: false
        };
    }

    componentDidMount() {
        const {nutritionInfo, backgroundsInfo, authInfo} = this.props;
        if (nutritionInfo.nutrition.length === 0) {
            const {backgrounds} = backgroundsInfo;
            const {token} = authInfo;

            this.props.getNutrition(backgrounds, token);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {nutritionInfo} = this.props;
        const {
            dataChanged,
            dataDeleted,
            error
        } = nutritionInfo;

        if (dataChanged) this.updateSuccessHandle();
        else if (dataDeleted) this.deleteSuccessHandle();
        else if (error) this.operationErrorHandle();
    }

    updateSuccessHandle = () => {
        this.props.nutritionChangeHandle();
        this.setState({
            isSubmitting: false,
            isEditing: false
        });

        toast.success("Блюдо успешно обновлено!");
    }

    deleteSuccessHandle = () => {
        this.props.nutritionChangeHandle();
        this.setState({
            isSubmitting: false,
            deleted: true
        });

        toast.success("Блюдо удалено успешно!");
    }

    operationErrorHandle = () => {
        this.props.nutritionChangeHandle();
        this.setState({isSubmitting: false});

        toast.error("Ошибка при выполнении операции!");
    }

    setBackgroundBase64 = backgroundBase64 => {
        this.setState({backgroundBase64: backgroundBase64});
    }

    onUpdateSubmit = values => {
        const {name, description, recipe, ingredients} = values;
        const {backgroundBase64} = this.state;

        const {nutrition, authInfo} = this.props;
        const {id, backgroundId} = nutrition;
        const {userId, token} = authInfo;

        const ingredientsForFetch = ingredients.map(ingredients => ({...ingredients, userId}));
        const size = ingredients.length >= 2 ? "2x2" : "2x1";

        const updatedNutrition = {
            id,
            name,
            description,
            recipe,
            ingredients: ingredientsForFetch,
            backgroundId,
            background: backgroundBase64,
            size,
            userId
        };

        this.props.updateNutrition(updatedNutrition, token);
        this.setState({isSubmitting: true});
    }

    onDeleteSubmit = () => {
        const {nutrition, authInfo} = this.props;
        const {id} = nutrition;
        const {token} = authInfo;

        this.props.deleteNutrition(id, token);
        this.setState({isSubmitting: true});
    }

    editingHandle = () => {
        const {isEditing} = this.state;
        this.setState({
            backgroundBase64: "",
            isEditing: !isEditing
        });
    }

    redirectToMainHandle = () => {
        this.setState({redirectToMain: true});
    }

    render() {
        const {
            isSubmitting,
            isEditing,
            deleted,
            redirectToMain
        } = this.state;
        if (deleted || redirectToMain) return <Redirect push to="/nutrition" />;

        const {nutrition} = this.props;
        const {name, description, recipe, ingredients, background} = nutrition;

        const editingProps = {
            isSubmitting,
            setBackgroundBase64: this.setBackgroundBase64,
            initialValues: {name, description, recipe, ingredients},
            onUpdateSubmit: this.onUpdateSubmit,
            cancelHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };
        const infoProps = {
            isSubmitting,
            name,
            description,
            recipe,
            ingredients,
            onDeleteSubmit: this.onDeleteSubmit,
            editingHandle: this.editingHandle,
            redirectToMainHandle: this.redirectToMainHandle
        };

        const innerComponent = isEditing ? <EditingForm {...editingProps} /> : <InfoForm {...infoProps} />;
        return <DefiniteNutrition innerComponent={innerComponent} background={background} />;
    }
}

export default DefiniteNutritionContainer;