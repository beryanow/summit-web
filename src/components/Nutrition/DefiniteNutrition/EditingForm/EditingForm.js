import React from 'react';
import { Formik, Form } from 'formik';

import EditingField from './EditingField/EditingField';
import EditingFieldArray from './EditingFieldArray/EditingFieldArray';
import EditingButtons from './EditingButtons/EditingButtons';
import EditingImageUploadContainer from './EditingImageUpload/EditingImageUploadContainer';

import classes from './EditingForm.module.css';


const validate = (values) => {
    const validateIngredientsArr = ingredients => {
        const ingredientsError = [];
        let isErrorsExist = false;

        ingredients.forEach((ingredient) => {
            const {name, amount} = ingredient;
            const errorObject = {};

            if (!name) errorObject.name = "Не заполнено";
            if (!amount) errorObject.amount = "Не заполнено";

            isErrorsExist = Object.keys(errorObject).length !== 0;
            ingredientsError.push(errorObject);
        });

        return isErrorsExist ? ingredientsError : null;
    }

    const errors = {};
    const valuesArr = Object.entries(values);

    for (const [key, value] of valuesArr) {
        if (key === "ingredients") {
            const ingredientsError = validateIngredientsArr(value);
            if (ingredientsError) errors.ingredients = ingredientsError;
            continue;
        }
        if (!value) errors[key] = "Не заполнено";
    }

    return errors;
}

const EditingForm = ({
    isSubmitting,
    setBackgroundBase64,
    initialValues,
    onUpdateSubmit,
    cancelHandle,
    redirectToMainHandle
}) => (
    <Formik
        initialValues={initialValues}
        validate={validate}
        onSubmit={onUpdateSubmit}
    >
        {({values}) => (
            <Form className={classes.editingForm}>
                <span className={classes.editingFormTitle}>Обновление блюда</span>
                <EditingField
                    id="nutrition-update-name"
                    name="name"
                    label="Название блюда"
                />
                <EditingField
                    id="nutrition-update-description"
                    name="description"
                    label="Описание блюда"
                />
                <EditingField
                    id="nutrition-update-recipe"
                    name="recipe"
                    label="Рецепт блюда"
                />
                <EditingImageUploadContainer
                    setBackgroundBase64={setBackgroundBase64}
                />
                <EditingFieldArray
                    isSubmitting={isSubmitting}
                    ingredients={values.ingredients}
                />
                <EditingButtons
                    isSubmitting={isSubmitting}
                    cancelHandle={cancelHandle}
                    redirectToMainHandle={redirectToMainHandle}
                />
            </Form>
        )}
    </Formik>
);

export default EditingForm;
