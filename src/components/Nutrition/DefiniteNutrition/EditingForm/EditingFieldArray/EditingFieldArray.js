import React from 'react';
import { FieldArray } from 'formik';

import IngredientField from './IngredientField/IngredientField';
import classes from './EditingFieldArray.module.css';


const EditingFieldArray = ({ isSubmitting, ingredients }) => (
    <FieldArray name="ingredients" >
        {({ push, remove }) => (
            <div className={classes.editingFieldArray}>
                {
                    ingredients.length > 0
                    &&
                    <span className={classes.editingFieldArrayTitle}>Ингредиенты</span>
                }
                {ingredients.map((ingredient, index) => (
                    <div key={index} className={classes.editingFieldArrayElement}>
                        <IngredientField
                            id={`ingredients-update-name-${index}`}
                            name={`ingredients[${index}].name`}
                            label="Ингредиент"
                        />
                        <IngredientField
                            id={`ingredients-update-amount-${index}`}
                            name={`ingredients[${index}].amount`}
                            label="Количество"
                        />
                        <button type="button" onClick={() => remove(index)} disabled={isSubmitting}>
                            Удалить
                        </button>
                    </div>
                ))}
                <button type="button" onClick={() => push({name: "", amount: ""})} disabled={isSubmitting}>
                    Добавить ингредиент
                </button>
            </div>
        )}
    </FieldArray>


);

export default EditingFieldArray;
