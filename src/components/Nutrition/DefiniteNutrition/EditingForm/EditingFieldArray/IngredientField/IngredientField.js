import React from 'react';
import { Field, ErrorMessage } from 'formik';

import classes from './IngredientField.module.css';


const IngredientField = ({ id, name, label }) => (
    <div className={classes.ingredientField}>
        <label
            htmlFor={id}
            className={classes.ingredientFieldLabel}
        >
            {label}
        </label>
        <Field
            id={id}
            type="text"
            name={name}
            autoComplete="off"
            className={classes.ingredientFieldInput}
        />
        <ErrorMessage
            name={name}
            component="div"
            className={classes.ingredientFieldError}
        />
    </div>
);

export default IngredientField;
