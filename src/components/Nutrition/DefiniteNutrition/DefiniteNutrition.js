import React from 'react';
import classes from './DefiniteNutrition.module.css';


const DefiniteNutrition = ({innerComponent, background}) => {
    const backgroundImageStyle = {backgroundImage: `url(data:image/png;base64,${background})`};

    return (
        <div className={classes.definiteNutrition} style={backgroundImageStyle}>
            {innerComponent}
        </div>
    );
}

export default DefiniteNutrition;
