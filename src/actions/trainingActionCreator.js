import {
    SERVER,
    GET_TRAINING_SUCCESS,
    GET_TRAINING_ERROR,
    GET_ALL_EXERCISES_SUCCESS,
    GET_ALL_EXERCISES_ERROR,
    GET_DEFAULT_TRAINING_SUCCESS,
    GET_DEFAULT_TRAINING_ERROR,
    CREATE_TRAINING_SUCCESS,
    CREATE_TRAINING_ERROR,
    UPDATE_TRAINING_SUCCESS,
    UPDATE_TRAINING_ERROR,
    DELETE_TRAINING_SUCCESS,
    DELETE_TRAINING_ERROR,
    TRAINING_CHANGE_HANDLE,
    TRAINING_REQUEST
} from '../constants';


const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}

export const getTrainingFetch = (backgrounds, token) => {
    const createTrainingWithBackgroundProperty = (trainingWithoutBackground) => {
        const trainingWithBackground = [];
        for (const trainingWithoutBackgroundElement of trainingWithoutBackground) {
            const {backgroundId} = trainingWithoutBackgroundElement;
            const potentialBackground = backgrounds.get(backgroundId);

            const newTrainingWithoutBackgroundElement = potentialBackground ?
                {...trainingWithoutBackgroundElement, background: potentialBackground} :
                {...trainingWithoutBackgroundElement, background: ""};
            trainingWithBackground.push(newTrainingWithoutBackgroundElement);
        }

        return trainingWithBackground;
    }

    return dispatch => {
        const url = `${SERVER}/trainings`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(trainingWithoutBackground => {
                const trainingWithBackground = createTrainingWithBackgroundProperty(trainingWithoutBackground);
                dispatch(getTrainingSuccess(trainingWithBackground, backgrounds));
            })
            .catch(error => dispatch(getTrainingError(error)));
    }
}

/*
const getTrainingBackgroundFetch = (training, backgrounds, token) => {
    return dispatch => {
        const backgroundFetches = [];

        for (const trainingElement of training) {
            const {backgroundId} = trainingElement;
            if (backgrounds.has(backgroundId)) continue;

            const url = `${SERVER}/image?id=${backgroundId}`;
            const request = new Request(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(backgroundId, image.content);
                        trainingElement.background = image.content;
                    })
                    .catch(error => dispatch(getTrainingError(error)))
            );
        }

        Promise.all(backgroundFetches)
            .then(() => dispatch(getTrainingSuccess(training, backgrounds)))
            .catch(error => dispatch(getTrainingError(error)));
    }
}
 */

const getTrainingSuccess = (training, backgrounds) => ({
    type: GET_TRAINING_SUCCESS,
    payload: {
        training,
        backgrounds
    }
})

const getTrainingError = error => ({
    type: GET_TRAINING_ERROR,
    payload: {
        error
    }
})

export const getAllExerciseFetch = (token) =>{
    return dispatch =>{
        const url = `${SERVER}/exercise/getAll`;
        const request = new Request(url, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(exercises => dispatch(getAllExerciseSuccess(exercises)))
            .catch(error => dispatch(getAllExerciseError(error)));
    }
}

const getAllExerciseSuccess = (exercises) => ({
    type: GET_ALL_EXERCISES_SUCCESS,
    payload: {
        exercises
    }
})

const getAllExerciseError = (error) => ({
    type: GET_ALL_EXERCISES_ERROR,
    payload:{
        error
    }
})

export const getDefaultTrainingFetch = (token, type) => {
    const getDefaultTraining = () => {
        const url = `${SERVER}/training/default/${type}`;
        const request = new Request(url, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${token}`
            }
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const createTraining = (trainingDTO) => {
        const url = `${SERVER}/training`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(trainingDTO)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }


    return dispatch => {
        getDefaultTraining()
            .then(defaultTraining => {
                return createTraining(defaultTraining);
            })
            .then(training => dispatch(createTrainingSuccess(training)))
            .catch(error => dispatch(createTrainingError(error)));
    }
}

const getDefaultTrainingSuccess = (training) => ({
    type: GET_DEFAULT_TRAINING_SUCCESS,
    payload: {
        training
    }
})

const getDefaultTrainingError = (error) => ({
    type: GET_DEFAULT_TRAINING_ERROR,
    payload:{
        error
    }
})

export const createTrainingFetch = (training, token) => {
    return dispatch => {
        const url = `${SERVER}/training`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(training)
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(training => dispatch(createTrainingSuccess(training)))
            .catch(error => {
                dispatch(createTrainingError(error))
            });
    }
}

const createTrainingSuccess = (training) => ({
    type: CREATE_TRAINING_SUCCESS,
    payload: {
        training
    }
})

const createTrainingError = (error) => ({
    type: CREATE_TRAINING_ERROR,
    payload: {
        error
    }
})

export const updateTrainingFetch = (training, token) => {
    return dispatch => {
        const url = `${SERVER}/training`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(training)
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(training => dispatch(updateTrainingSuccess(training)))
            .catch(error => dispatch(updateTrainingError(error)));
    }
}

const updateTrainingSuccess = (training) => ({
    type: UPDATE_TRAINING_SUCCESS,
    payload: {
        training
    }
})

const updateTrainingError = (error) => ({
    type: UPDATE_TRAINING_ERROR,
    payload: {
        error
    }
})

export const deleteTrainingFetch = (id, token) => {
    return dispatch => {
        const url = `${SERVER}/training/delete/${id}`;
        const request = new Request(url, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(_ => dispatch(deleteTrainingSuccess(id)))
            .catch(error => dispatch(deleteTrainingError(error)));
    }
}

const deleteTrainingSuccess = (id) => ({
    type: DELETE_TRAINING_SUCCESS,
    payload: {
        id
    }
})

const deleteTrainingError = (error) => ({
    type: DELETE_TRAINING_ERROR,
    payload: {
        error
    }
})

export const trainingChangeHandle = () => ({
    type: TRAINING_CHANGE_HANDLE,
    payload: {}
})

const trainingRequest = () => ({
    type: TRAINING_REQUEST,
    payload: {}
})