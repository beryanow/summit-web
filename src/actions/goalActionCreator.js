import {
    SERVER,
    GET_GOALS_SUCCESS,
    GET_BACKGROUNDS_SUCCESS,
    GET_GOALS_ERROR,
    CREATE_GOAL_SUCCESS,
    CREATE_GOAL_ERROR,
    UPDATE_GOAL_SUCCESS,
    UPDATE_GOAL_ERROR,
    DELETE_GOAL_SUCCESS,
    DELETE_GOAL_ERROR,
    GOAL_CHANGE_HANDLE
} from '../constants';


const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}


export const getGoalsFetch = (backgrounds, token) => {
    const createGoalsWithBackgroundProperty = (goalsWithoutBackground) => {
        const goalsWithBackground = [];
        for (const goal of goalsWithoutBackground) {
            const {backgroundId} = goal;
            const potentialBackground = backgrounds.get(backgroundId);

            const newGoal = potentialBackground ? {...goal, background: potentialBackground} : {...goal, background: ""};
            goalsWithBackground.push(newGoal);
        }

        return goalsWithBackground;
    }

    return dispatch => {
        const url = `${SERVER}/goal/getAll`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(goalsWithoutBackground => {
                const goalsWithBackground = createGoalsWithBackgroundProperty(goalsWithoutBackground);
                dispatch(getGoalsSuccess(goalsWithBackground));
                dispatch(getGoalsBackgroundFetch(goalsWithBackground, backgrounds, token));
            })
            .catch(error => dispatch(getGoalsError(error)));
    }
}

const getGoalsBackgroundFetch = (goals, backgrounds, token) => {
    return dispatch => {
        const backgroundFetches = [];

        for (const goal of goals) {
            const {backgroundId} = goal;
            if (backgrounds.has(backgroundId)) continue;

            const url = `${SERVER}/image?id=${backgroundId}`;
            const request = new Request(url, {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(backgroundId, image.content);
                        goal.background = image.content;
                    })
                    .catch(error => dispatch(getGoalsError(error)))
            )
        }

        Promise.all(backgroundFetches)
            .then(() => {
                dispatch(getGoalsSuccess(goals));
                dispatch(getBackgroundsSuccess(backgrounds));
            })
            .catch(error => dispatch(getGoalsError(error)));
    }
}

const getGoalsSuccess = (goals) => ({
    type: GET_GOALS_SUCCESS,
    payload: {
        goals
    }
})

const getBackgroundsSuccess = (backgrounds) => ({
    type: GET_BACKGROUNDS_SUCCESS,
    payload: {
        backgrounds
    }
})

const getGoalsError = (error) => ({
    type: GET_GOALS_ERROR,
    payload: {
        error
    }
})

export const createGoalFetch = (goal, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const createGoal = goalWithoutBackgroundField => {
        const url = `${SERVER}/goal`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(goalWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...goalWithoutBackgroundField} = goal;

        if (background) {
            createImage(background)
                .then(image => {
                    goalWithoutBackgroundField.backgroundId = image.id;
                    return createGoal(goalWithoutBackgroundField);
                })
                .then(goal => dispatch(createGoalSuccess(goal)))
                .catch(error => dispatch(createGoalError(error)));
        } else {
            createGoal(goalWithoutBackgroundField)
                .then(goal => dispatch(createGoalSuccess(goal)))
                .catch(error => dispatch(createGoalError(error)));
        }
    }
}

const createGoalSuccess = (goal) => ({
    type: CREATE_GOAL_SUCCESS,
    payload: {
        goal
    }
})

const createGoalError = (error) => ({
    type: CREATE_GOAL_ERROR,
    payload: {
        error
    }
})

export const updateGoalFetch = (goal, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const updateGoal = goalWithoutBackgroundField => {
        const url = `${SERVER}/goal`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(goalWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...goalWithoutBackgroundField} = goal;

        if (background) {
            createImage(background)
                .then(image => {
                    goalWithoutBackgroundField.backgroundId = image.id;
                    return updateGoal(goalWithoutBackgroundField);
                })
                .then(goal => dispatch(updateGoalSuccess(goal)))
                .catch(error => dispatch(updateGoalError(error)));
        } else {
            updateGoal(goalWithoutBackgroundField)
                .then(goal => dispatch(updateGoalSuccess(goal)))
                .catch(error => dispatch(updateGoalError(error)));
        }
    }
}

const updateGoalSuccess = (goal) => ({
    type: UPDATE_GOAL_SUCCESS,
    payload: {
        goal
    }
})

const updateGoalError = (error) => ({
    type: UPDATE_GOAL_ERROR,
    payload: {
        error
    }
})

export const deleteGoalFetch = (id, token) => {
    return dispatch => {
        const url = `${SERVER}/goal/delete/${id}`;
        const request = new Request(url, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(_ => dispatch(deleteGoalSuccess(id)))
            .catch(error => dispatch(deleteGoalError(error)));
    }
}

const deleteGoalSuccess = (id) => ({
    type: DELETE_GOAL_SUCCESS,
    payload: {
        id
    }
})

const deleteGoalError = (error) => ({
    type: DELETE_GOAL_ERROR,
    payload: {
        error
    }
})

export const goalChangeHandle = () => ({
    type: GOAL_CHANGE_HANDLE,
    payload: {}
})