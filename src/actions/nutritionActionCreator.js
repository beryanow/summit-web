import {
    SERVER,
    GET_NUTRITION_SUCCESS,
    GET_BACKGROUNDS_SUCCESS,
    GET_NUTRITION_ERROR,
    CREATE_NUTRITION_SUCCESS,
    CREATE_NUTRITION_ERROR,
    NUTRITION_CHANGE_HANDLE,
    NUTRITION_REQUEST,
    UPDATE_NUTRITION_SUCCESS,
    UPDATE_NUTRITION_ERROR,
    DELETE_NUTRITION_SUCCESS,
    DELETE_NUTRITION_ERROR
} from '../constants';


const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}

export const getNutritionFetch = (backgrounds, token) => {
    const createNutritionWithBackgroundProperty = (nutritionWithoutBackground) => {
        const nutritionWithBackground = [];
        for (const nutritionWithoutBackgroundElement of nutritionWithoutBackground) {
            const {backgroundId} = nutritionWithoutBackgroundElement;
            const potentialBackground = backgrounds.get(backgroundId);

            const newNutritionWithoutBackgroundElement = potentialBackground ?
                {...nutritionWithoutBackgroundElement, background: potentialBackground} :
                {...nutritionWithoutBackgroundElement, background: ""};
            nutritionWithBackground.push(newNutritionWithoutBackgroundElement);
        }

        return nutritionWithBackground;
    }

    return dispatch => {
        const url = `${SERVER}/nutrition/getAll`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(nutritionWithoutBackground => {
                const nutritionWithBackground = createNutritionWithBackgroundProperty(nutritionWithoutBackground);
                dispatch(getNutritionSuccess(nutritionWithBackground));
                dispatch(getNutritionBackgroundFetch(nutritionWithBackground, backgrounds, token))
            })
            .catch(error => dispatch(getNutritionError(error)));
    }
}

const getNutritionBackgroundFetch = (nutrition, backgrounds, token) => {
    return dispatch => {
        const backgroundFetches = [];

        for (const nutritionElement of nutrition) {
            const {backgroundId} =nutritionElement;
            if (backgrounds.has(backgroundId)) continue;

            const url = `${SERVER}/image?id=${backgroundId}`;
            const request = new Request(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(backgroundId, image.content);
                        nutritionElement.background = image.content;
                    })
                    .catch(error => dispatch(getNutritionError(error)))
            );
        }

        Promise.all(backgroundFetches)
            .then(() => {
                dispatch(getNutritionSuccess(nutrition));
                dispatch(getBackgroundsSuccess(backgrounds));
            })
            .catch(error => dispatch(getNutritionError(error)));
    }
}

const getNutritionSuccess = (nutrition) => ({
    type: GET_NUTRITION_SUCCESS,
    payload: {
        nutrition
    }
})

const getBackgroundsSuccess = (backgrounds) => ({
    type: GET_BACKGROUNDS_SUCCESS,
    payload: {
        backgrounds
    }
})

const getNutritionError = error => ({
    type: GET_NUTRITION_ERROR,
    payload: {
        error
    }
})

export const createNutritionFetch = (nutrition, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const createNutrition = nutritionWithoutBackgroundField => {
        const url = `${SERVER}/nutrition`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(nutritionWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...nutritionWithoutBackgroundField} = nutrition;

        if (background) {
            createImage(background)
                .then(image => {
                    nutritionWithoutBackgroundField.backgroundId = image.id;
                    return createNutrition(nutritionWithoutBackgroundField);
                })
                .then(nutrition => dispatch(createNutritionSuccess(nutrition)))
                .catch(error => dispatch(createNutritionError(error)));
        } else {
            createNutrition(nutritionWithoutBackgroundField)
                .then(nutrition => dispatch(createNutritionSuccess(nutrition)))
                .catch(error => dispatch(createNutritionError(error)));
        }
    }
}

const createNutritionSuccess = (nutrition) => ({
    type: CREATE_NUTRITION_SUCCESS,
    payload: {
        nutrition
    }
})

const createNutritionError = (error) => ({
    type: CREATE_NUTRITION_ERROR,
    payload: {
        error
    }
})

export const updateNutritionFetch = (nutrition, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const updateNutrition = nutritionWithoutBackgroundField => {
        const url = `${SERVER}/nutrition`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(nutritionWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...nutritionWithoutBackgroundField} = nutrition;

        if (background) {
            createImage(background)
                .then(image => {
                    nutritionWithoutBackgroundField.backgroundId = image.id;
                    return updateNutrition(nutritionWithoutBackgroundField);
                })
                .then(nutrition => dispatch(updateNutritionSuccess(nutrition)))
                .catch(error => dispatch(updateNutritionError(error)));
        } else {
            updateNutrition(nutritionWithoutBackgroundField)
                .then(nutrition => dispatch(updateNutritionSuccess(nutrition)))
                .catch(error => dispatch(updateNutritionError(error)));
        }
    }
}

const updateNutritionSuccess = (nutrition) => ({
    type: UPDATE_NUTRITION_SUCCESS,
    payload: {
        nutrition
    }
})

const updateNutritionError = (error) => ({
    type: UPDATE_NUTRITION_ERROR,
    payload: {
        error
    }
})

export const deleteNutritionFetch = (id, token) => {
    return dispatch => {
        const url = `${SERVER}/nutrition/delete/${id}`;
        const request = new Request(url, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(_ => dispatch(deleteNutritionSuccess(id)))
            .catch(error => dispatch(deleteNutritionError(error)));
    }
}

const deleteNutritionSuccess = (id) => ({
    type: DELETE_NUTRITION_SUCCESS,
    payload: {
        id
    }
})

const deleteNutritionError = (error) => ({
    type: DELETE_NUTRITION_ERROR,
    payload: {
        error
    }
})

export const nutritionChangeHandle = () => ({
    type: NUTRITION_CHANGE_HANDLE,
    payload: {}
})

const nutritionRequest = () => ({
    type: NUTRITION_REQUEST,
    payload: {}
})