import {
    SIGN_IN_SUCCESS,
    SIGN_IN_ERROR,
    SIGN_UP_SUCCESS,
    SIGN_UP_ERROR,
    AUTH_CHANGE_HANDLE
} from '../constants';


const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VybmFtZSIsImV4cCI6MTYwOTQzMjEwM30.c81LzIDIpCAJALRZvs99G8dA0oSFNdu6jVnThXarWQEvj-W6NR_xSIGDPJ0OCN-yzHBcjJAMrDQ-VSo32LRRXA";
const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}


export const signInFetch = (username, password) => {
    return dispatch => {
        const url = `http://thesummit.herokuapp.com/login`;
        const body = { username: username, password: password };
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(body)
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => dispatch(signInSuccess(token)))
            .catch(error => dispatch(signInError(error)));
    }
}

const signInSuccess = token => ({
    type: SIGN_IN_SUCCESS,
    payload: {
        token
    }
})

const signInError = error => ({
    type: SIGN_IN_ERROR,
    payload: {
        error
    }
})

export const signUpFetch = (dataForSignUp) => {
    return dispatch => {
        const url = `http://thesummit.herokuapp.com/auth/register`;
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(dataForSignUp)
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => dispatch(signUpSuccess()))
            .catch(error => dispatch(signUpError(error)));
    }
}

const signUpSuccess = () => ({
    type: SIGN_UP_SUCCESS,
    payload: {}
})

const signUpError = (error) => ({
    type: SIGN_UP_ERROR,
    payload: {
        error
    }
})

export const authChangeHandle = () => ({
    type: AUTH_CHANGE_HANDLE,
    payload: {}
})