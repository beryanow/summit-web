import {
    SERVER,
    GET_PROFILE_SUCCESS,
    GET_BACKGROUNDS_SUCCESS,
    GET_PROFILE_ERROR,
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_ERROR,
    CREATE_PROGRESS_SUCCESS,
    CREATE_PROGRESS_ERROR,
    GET_PROGRESS_SUCCESS,
    GET_PROGRESS_ERROR,
    DELETE_ALL_PROGRESS_SUCCESS,
    DELETE_ALL_PROGRESS_ERROR,
    PROFILE_CHANGE_HANDLE
} from '../constants';


const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}


export const getProfileFetch = (backgrounds, token) => {
    // Создаем список из id тех background, которые нужно взять с сервера
    const createBackgroundIdList = profile => {
        const backgroundIdSet = new Set();
        backgroundIdSet.add(profile.imageId);

        for (const [key, value] of Object.entries(profile.settingsDto)) {
            if (key.includes("BackgroundId") && !backgroundIdSet.has(value)) {
                backgroundIdSet.add(value);
            }
        }

        return [...backgroundIdSet];
    }

    return dispatch => {
        const url = `${SERVER}/profile`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(profile => {
                const backgroundIdList = createBackgroundIdList(profile);

                const backgroundField = backgrounds.has(profile.imageId) ? backgrounds.get(profile.imageId) : "";
                const profileWithBackground = {...profile, background: backgroundField};

                dispatch(getProfileSuccess(profileWithBackground));
                dispatch(getBackgroundsFetch(profileWithBackground, backgroundIdList, backgrounds, token));
            })
            .catch(error => dispatch(getProfileError(error)));
    }
}

const getBackgroundsFetch = (profile, backgroundIdList, backgrounds, token) => {
    // backgroundIdList - коллекция, содержащая backgroundId из профиля
    // backgrounds - коллекция, содержащая все картинки, которые задействованы в приложении
    return dispatch => {
        const backgroundFetches = [];
        const imageId = backgroundIdList[0];

        for (const backgroundId of backgroundIdList) {
            if (backgrounds.has(backgroundId)) continue;

            const url = `${SERVER}/image?id=${backgroundId}`;
            const request = new Request(url, {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(backgroundId, image.content);
                        if (image.id === imageId) profile.background = image.content;
                    })
                    .catch(error => dispatch(getProfileError(error)))
            );
        }

        Promise.all(backgroundFetches)
            .then(() => {
                dispatch(getProfileSuccess(profile));
                dispatch(getBackgroundsSuccess(backgrounds));
            })
            .catch(error => dispatch(getProfileError(error)));
    }
}

const getProfileSuccess = profile => ({
    type: GET_PROFILE_SUCCESS,
    payload: {
        profile
    }
})

const getBackgroundsSuccess = backgrounds => ({
    type: GET_BACKGROUNDS_SUCCESS,
    payload: {
        backgrounds
    }
})

const getProfileError = error => ({
    type: GET_PROFILE_ERROR,
    payload: {
        error
    }
})

export const updateProfileFetch = (profile, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const updateProfile = profileWithoutBackgroundField => {
        const url = `${SERVER}/profile`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(profileWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...profileWithoutBackgroundField} = profile;

        if (background) {
            createImage(background)
                .then(image => {
                    profileWithoutBackgroundField.imageId = image.id;
                    return updateProfile(profileWithoutBackgroundField);
                })
                .then(profile => dispatch(updateProfileSuccess(profile)))
                .catch(error => dispatch(updateProfileError(error)));
        } else {
            updateProfile(profileWithoutBackgroundField)
                .then(profile => dispatch(updateProfileSuccess(profile)))
                .catch(error => dispatch(updateProfileError(error)));
        }
    }
}

const updateProfileSuccess = profile => ({
    type: UPDATE_PROFILE_SUCCESS,
    payload: {
        profile
    }
})

const updateProfileError = error => ({
    type: UPDATE_PROFILE_ERROR,
    payload: {
        error
    }
})


export const createProgressFetch = (progress, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const createProgress = progressWithoutImageField => {
        const url = `${SERVER}/profile/progress`;
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(progressWithoutImageField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {image, ...progressWithoutImageField} = progress;
        createImage(image)
            .then(image => {
                progressWithoutImageField.imageId = image.id;
                return createProgress(progressWithoutImageField);
            })
            .then(_ => dispatch(createProgressSuccess()))
            .catch(error => dispatch(createProgressError(error)));
    }
}

const createProgressSuccess = () => ({
    type: CREATE_PROGRESS_SUCCESS,
    payload: {}
})

const createProgressError = error => ({
    type: CREATE_PROGRESS_ERROR,
    payload: {
        error
    }
})


export const getProgressFetch = (backgrounds, token) => {
    const createProgressWithBackgroundProperty = progressWithoutBackground => {
        const progressWithBackground = [];

        for (const progressElement of progressWithoutBackground) {
            const { imageId } = progressElement;
            const potentialImage = backgrounds.get(imageId);

            const newProgressElement = potentialImage ?
                {...progressElement, image: potentialImage} :
                {...progressElement, image: ""};

            progressWithBackground.push(newProgressElement);
        }

        return progressWithBackground;
    }

    return dispatch => {
        const url = `${SERVER}/profile/progress`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(progressWithoutBackground => {
                const progressWithBackground = createProgressWithBackgroundProperty(progressWithoutBackground);
                dispatch(getProgressSuccess(progressWithBackground));
                dispatch(getProgressBackgroundFetch(progressWithBackground, backgrounds, token));
            })
            .catch(error => dispatch(getProgressError(error)));
    }
}

const getProgressBackgroundFetch = (progress, backgrounds, token) => {
    return dispatch => {
        const backgroundFetches = [];

        for (const progressElement of progress) {
            const { imageId } = progressElement;
            if (backgrounds.has(imageId)) continue;

            const url = `${SERVER}/image?id=${imageId}`;
            const request = new Request(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(imageId, image.content);
                        progressElement.image = image.content;
                    })
                    .catch(error => dispatch(getProgressError(error)))
            );
        }

        Promise.all(backgroundFetches)
            .then(() => {
                dispatch(getProgressSuccess(progress));
                dispatch(getBackgroundsSuccess(backgrounds));
            })
            .catch(error => dispatch(getProgressError(error)));
    }
}

const getProgressSuccess = progress => ({
    type: GET_PROGRESS_SUCCESS,
    payload: {
        progress
    }
})

const getProgressError = error => ({
    type: GET_PROGRESS_ERROR,
    payload: {
        error
    }
})

export const deleteAllProgressFetch = token => {
    return dispatch => {
        const url = `${SERVER}/profile/progress/deleteAll`;
        const request = new Request(url, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(_ => dispatch(deleteAllProgressSuccess()))
            .catch(error => dispatch(deleteAllProgressError(error)));
    }
}

const deleteAllProgressSuccess = () => ({
    type: DELETE_ALL_PROGRESS_SUCCESS,
    payload: {}
})

const deleteAllProgressError = error => ({
    type: DELETE_ALL_PROGRESS_ERROR,
    payload: {
        error
    }
})

export const profileChangeHandle = () => ({
    type: PROFILE_CHANGE_HANDLE,
    payload: {}
})