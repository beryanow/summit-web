import {
    SERVER,
    GET_IDEAS_SUCCESS,
    GET_BACKGROUNDS_SUCCESS,
    GET_IDEAS_ERROR,
    CREATE_IDEA_SUCCESS,
    CREATE_IDEA_ERROR,
    UPDATE_IDEA_SUCCESS,
    UPDATE_IDEA_ERROR,
    DELETE_IDEA_SUCCESS,
    DELETE_IDEA_ERROR,
    IDEA_CHANGE_HANDLE
} from '../constants';


const handleNotOkResponse = response => {
    if (!response.ok) throw Error(response.statusText);
    return response;
}


export const getIdeasFetch = (backgrounds, token) => {
    const createIdeasWithBackgroundProperty = (ideasWithoutBackground) => {
        let ideasWithBackground = [];
        for (const idea of ideasWithoutBackground) {
            const {backgroundId} = idea;
            const potentialBackground = backgrounds.get(backgroundId);

            const newIdea = potentialBackground ? {...idea, background: potentialBackground} : {...idea, background: ""};
            ideasWithBackground.push(newIdea);
        }

        return ideasWithBackground;
    }

    return dispatch => {
        const url = `${SERVER}/idea/getAll`;
        const request = new Request(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json())
            .then(ideasWithoutBackground => {
                const ideasWithBackground = createIdeasWithBackgroundProperty(ideasWithoutBackground);
                dispatch(getIdeasSuccess(ideasWithBackground));
                dispatch(getIdeasBackgroundFetch(ideasWithBackground, backgrounds, token));
            })
            .catch(error => dispatch(getIdeasError(error)));
    }
}

const getIdeasBackgroundFetch = (ideas, backgrounds, token) => {
    return dispatch => {
        const backgroundFetches = [];

        for (const idea of ideas) {
            const {backgroundId} = idea;
            if (backgrounds.has(backgroundId)) continue;

            const url = `${SERVER}/image?id=${backgroundId}`;
            const request = new Request(url, {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            backgroundFetches.push(
                fetch(request)
                    .then(handleNotOkResponse)
                    .then(response => response.json())
                    .then(image => {
                        backgrounds.set(backgroundId, image.content);
                        idea.background = image.content;
                    })
                    .catch(error => dispatch(getIdeasError(error)))
            );
        }

        Promise.all(backgroundFetches)
            .then(() => {
                dispatch(getIdeasSuccess(ideas));
                dispatch(getBackgroundsSuccess(backgrounds));
            })
            .catch(error => dispatch(getIdeasError(error)));
    }
}

const getIdeasSuccess = (ideas) => ({
    type: GET_IDEAS_SUCCESS,
    payload: {
        ideas
    }
})

const getBackgroundsSuccess = (backgrounds) => ({
    type: GET_BACKGROUNDS_SUCCESS,
    payload: {
        backgrounds
    }
})

const getIdeasError = (error) => ({
    type: GET_IDEAS_ERROR,
    payload: {
        error
    }
})

export const createIdeaFetch = (idea, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const createIdea = ideaWithoutBackgroundField => {
        const url = `${SERVER}/idea`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(ideaWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...ideaWithoutBackgroundField} = idea;
        if (background) {
            createImage(background)
                .then(image => {
                    ideaWithoutBackgroundField.backgroundId = image.id;
                    return createIdea(ideaWithoutBackgroundField, dispatch);
                })
                .then(idea => dispatch(createIdeaSuccess(idea)))
                .catch(error => dispatch(createIdeaError(error)));
        } else {
            createIdea(ideaWithoutBackgroundField)
                .then(idea => dispatch(createIdeaSuccess(idea)))
                .catch(error => dispatch(createIdeaError(error)));
        }
    }
}

const createIdeaSuccess = (idea) => ({
    type: CREATE_IDEA_SUCCESS,
    payload: {
        idea
    }
})

const createIdeaError = (error) => ({
    type: CREATE_IDEA_ERROR,
    payload: {
        error
    }
})

export const updateIdeaFetch = (idea, token) => {
    const createImage = background => {
        const url = `${SERVER}/image`;
        const body = {content: background};
        const request = new Request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(body)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    const updateIdea = ideaWithoutBackgroundField => {
        const url = `${SERVER}/idea`;
        const request = new Request(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(ideaWithoutBackgroundField)
        });

        return fetch(request)
            .then(handleNotOkResponse)
            .then(response => response.json());
    }

    return dispatch => {
        const {background, ...ideaWithoutBackgroundField} = idea;
        if (background) {
            createImage(background)
                .then(image => {
                    ideaWithoutBackgroundField.backgroundId = image.id;
                    return updateIdea(ideaWithoutBackgroundField);
                })
                .then(idea => dispatch(updateIdeaSuccess(idea)))
                .catch(error => dispatch(updateIdeaError(error)));
        } else {
            updateIdea(ideaWithoutBackgroundField)
                .then(idea => dispatch(updateIdeaSuccess(idea)))
                .catch(error => dispatch(updateIdeaError(error)));
        }
    }
}

const updateIdeaSuccess = (idea) => ({
    type: UPDATE_IDEA_SUCCESS,
    payload: {
        idea
    }
})

const updateIdeaError = (error) => ({
    type: UPDATE_IDEA_ERROR,
    payload: {
        error
    }
})

export const deleteIdeaFetch = (id, token) => {
    return dispatch => {
        const url = `${SERVER}/ideas/delete/${id}`;
        const request = new Request(url, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        fetch(request)
            .then(response => {
                if (!response.ok) throw Error(response.statusText);
                return response;
            })
            .then(response => dispatch(deleteIdeaSuccess(id)))
            .catch(error => dispatch(deleteIdeaError(error)));
    }
}

const deleteIdeaSuccess = (id) => ({
    type: DELETE_IDEA_SUCCESS,
    payload: {
        id
    }
})

const deleteIdeaError = (error) => ({
    type: DELETE_IDEA_ERROR,
    payload: {
        error
    }
})


export const ideaChangeHandle = () => ({
    type: IDEA_CHANGE_HANDLE,
    payload: {}
})
