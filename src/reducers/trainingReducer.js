import {
    GET_TRAINING_SUCCESS,
    GET_TRAINING_ERROR,
    CREATE_TRAINING_SUCCESS,
    CREATE_TRAINING_ERROR,
    TRAINING_CHANGE_HANDLE,
    TRAINING_REQUEST,
    UPDATE_TRAINING_SUCCESS,
    UPDATE_TRAINING_ERROR,
    DELETE_TRAINING_SUCCESS,
    DELETE_TRAINING_ERROR,
    GET_ALL_EXERCISES_SUCCESS,
    GET_ALL_EXERCISES_ERROR,
    GET_DEFAULT_TRAINING_SUCCESS,
    GET_DEFAULT_TRAINING_ERROR
} from '../constants';


const initialState = {
    training: [],
    exercises: [],
    backgrounds: new Map(),
    isRequesting: false,
    dataChanged: false,
    dataDeleted: false,
    error: null
};

const trainingReducer = (state= initialState, { type, payload }) => {
    switch (type) {
        case GET_TRAINING_SUCCESS:
            return {
                ...state,
                training: payload.training,
                backgrounds: payload.backgrounds
            };
        case GET_TRAINING_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case CREATE_TRAINING_SUCCESS:
            return {
                ...state,
                training: [...state.training, payload.training],
                dataChanged: true
            };
        case CREATE_TRAINING_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case GET_ALL_EXERCISES_SUCCESS:
            return {
                ...state,
                exercises: payload.exercises
            };
        case GET_ALL_EXERCISES_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case GET_DEFAULT_TRAINING_SUCCESS:
            return {
                ...state,
                training: payload.training,
            };
        case GET_DEFAULT_TRAINING_ERROR:
            return{
                ...state,
                error: payload.error
            };
        case UPDATE_TRAINING_SUCCESS:
            const {id, name, cycle, exercises, trainingSchedules} = payload.training;

            const updatedTraining = state.training.map(trainingElement => {
                if (trainingElement.id === id) {
                    trainingElement.name = name;
                    trainingElement.cycle = cycle;
                    trainingElement.exercises = exercises;
                    trainingElement.trainingSchedules = trainingSchedules;
                }
                return trainingElement;
            });

            return {
                ...state,
                training: updatedTraining,
                dataChanged: true
            };
        case UPDATE_TRAINING_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case DELETE_TRAINING_SUCCESS:
            const trainingWithoutDeleted = state.training.filter(trainingElement =>
                trainingElement.id !== payload.id
            );
            return {
                ...state,
                training: trainingWithoutDeleted,
                dataDeleted: true
            };
        case DELETE_TRAINING_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case TRAINING_CHANGE_HANDLE:
            return {
                ...state,
                dataChanged: false,
                dataDeleted: false,
                error: null
            };
        case TRAINING_REQUEST:
            return {
                ...state,
                isRequesting: true
            };
        default:
            return state;
    }
}

export default trainingReducer;