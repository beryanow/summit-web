import { combineReducers } from 'redux';

import goalReducer from './goalReducer';
import ideaReducer from './ideaReducer';
import nutritionReducer from './nutritionReducer';
import profileReducer from './profileReducer';
import backgroundReducer from './backgroundReducer';
import authReducer from './authReducer';
import trainingReducer from "./trainingReducer";


export default combineReducers({
    goalsInfo: goalReducer,
    ideasInfo: ideaReducer,
    nutritionInfo: nutritionReducer,
    profileInfo: profileReducer,
    backgroundsInfo: backgroundReducer,
    trainingInfo: trainingReducer,
    authInfo: authReducer
});