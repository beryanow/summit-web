import {
    GET_BACKGROUNDS_SUCCESS
} from '../constants';


const initialState = {
    backgrounds: new Map()
}

const backgroundReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case GET_BACKGROUNDS_SUCCESS:
            return {
                ...state,
                backgrounds: payload.backgrounds
            };
        default:
            return state;
    }
}

export default backgroundReducer;

