import {
    GET_PROFILE_SUCCESS,
    GET_PROFILE_ERROR,
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_ERROR,
    CREATE_PROGRESS_SUCCESS,
    CREATE_PROGRESS_ERROR,
    GET_PROGRESS_SUCCESS,
    GET_PROGRESS_ERROR,
    DELETE_ALL_PROGRESS_SUCCESS,
    DELETE_ALL_PROGRESS_ERROR,
    PROFILE_CHANGE_HANDLE
} from '../constants';


const initialState = {
    profile: {},
    progress: [],
    dataChanged: false,
    dataDeleted: false,
    error: null
}

const profileReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_PROFILE_SUCCESS:
            return {
                ...state,
                profile: payload.profile
            };
        case GET_PROFILE_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case UPDATE_PROFILE_SUCCESS:
            return {
                ...state,
                profile: payload.profile,
                dataChanged: true
            };
        case UPDATE_PROFILE_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case CREATE_PROGRESS_SUCCESS:
            return {
                ...state,
                dataChanged: true
            };
        case CREATE_PROGRESS_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case GET_PROGRESS_SUCCESS:
            return {
                ...state,
                progress: payload.progress
            };
        case GET_PROGRESS_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case DELETE_ALL_PROGRESS_SUCCESS:
            return {
                ...state,
                progress: [],
                dataDeleted: true
            };
        case DELETE_ALL_PROGRESS_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case PROFILE_CHANGE_HANDLE:
            return {
                ...state,
                dataChanged: false,
                dataDeleted: false,
                error: null
            };
        default:
            return state;
    }
}

export default profileReducer;
