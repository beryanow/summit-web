import {
    GET_GOALS_SUCCESS,
    GET_GOALS_ERROR,
    CREATE_GOAL_SUCCESS,
    CREATE_GOAL_ERROR,
    UPDATE_GOAL_SUCCESS,
    UPDATE_GOAL_ERROR,
    DELETE_GOAL_SUCCESS,
    DELETE_GOAL_ERROR,
    GOAL_CHANGE_HANDLE
} from '../constants';


const initialState = {
    goals: [],
    isRequesting: false,
    dataChanged: false,
    dataDeleted: false,
    error: null
};

const goalReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case GET_GOALS_SUCCESS:
            return {
                ...state,
                goals: payload.goals
            };
        case GET_GOALS_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case CREATE_GOAL_SUCCESS:
            return {
                ...state,
                goals: [...state.goals, payload.goal],
                dataChanged: true
            };
        case CREATE_GOAL_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case UPDATE_GOAL_SUCCESS:
            const {id, name, description, tasks} = payload.goal;

            const updatedGoals = state.goals.map(goal => {
                if (goal.id === id) {
                    goal.name = name;
                    goal.description = description;
                    goal.tasks = tasks;
                }
                return goal;
            });

            return {
                ...state,
                goals: updatedGoals,
                dataChanged: true
            };
        case UPDATE_GOAL_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case DELETE_GOAL_SUCCESS:
            const goalsWithoutDeleted = state.goals.filter(goal => goal.id !== payload.id);
            return {
                ...state,
                goals: goalsWithoutDeleted,
                dataDeleted: true
            };
        case DELETE_GOAL_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case GOAL_CHANGE_HANDLE:
            return {
                ...state,
                dataChanged: false,
                dataDeleted: false,
                error: null
            };
        default:
            return state;
    }
}

export default goalReducer;