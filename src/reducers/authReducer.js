import {
    SIGN_IN_SUCCESS,
    SIGN_IN_ERROR,
    SIGN_UP_SUCCESS,
    SIGN_UP_ERROR,
    AUTH_CHANGE_HANDLE
} from '../constants';


const initialState = {
    userId: 1,
    token: "",
    isSuccess: false,
    error: null
};

const authReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SIGN_IN_SUCCESS:
            return {
                ...state,
                token: payload.token,
                isSuccess: true
            };
        case SIGN_IN_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                isSuccess: true
            };
        case SIGN_UP_ERROR:
            return {
                ...state,
                error: payload.error
            }
        case AUTH_CHANGE_HANDLE:
            return {
                ...state,
                isSuccess: false,
                error: null
            };
        default:
            return state;
    }
}

export default authReducer;