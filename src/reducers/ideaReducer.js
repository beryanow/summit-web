import {
    GET_IDEAS_SUCCESS,
    GET_IDEAS_ERROR,
    CREATE_IDEA_SUCCESS,
    CREATE_IDEA_ERROR,
    DELETE_IDEA_SUCCESS,
    DELETE_IDEA_ERROR,
    UPDATE_IDEA_SUCCESS,
    UPDATE_IDEA_ERROR,
    IDEA_CHANGE_HANDLE,
    IDEA_REQUEST
} from "../constants";


const initialState = {
    ideas: [],
    isRequesting: false,
    dataChanged: false,
    dataDeleted: false,
    error: null
}

const ideaReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_IDEAS_SUCCESS:
            return {
                ...state,
                ideas: payload.ideas,
            }
        case GET_IDEAS_ERROR:
            return {
                ...state,
                error: payload.error
            }
        case CREATE_IDEA_SUCCESS:
            return {
                ...state,
                ideas: [...state.ideas, payload.idea],
                dataChanged: true
            };
        case CREATE_IDEA_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case UPDATE_IDEA_SUCCESS:
            const {id, name, content} = payload.idea;

            const updatedIdeas = state.ideas.map(idea => {
                if (idea.id === id) {
                    idea.name = name;
                    idea.content = content;
                }
                return idea;
            });

            return {
                ...state,
                ideas: updatedIdeas,
                dataChanged: true
            }
        case UPDATE_IDEA_ERROR:
            return {
                ...state,
                error: payload.error
            }
        case DELETE_IDEA_SUCCESS:
            const ideasWithoutDeleted = state.ideas.filter(idea => idea.id !== payload.id);
            return {
                ...state,
                ideas: ideasWithoutDeleted,
                dataDeleted: true,
            };
        case DELETE_IDEA_ERROR:
            return {
                ...state,
                error: payload.error
            }
        case IDEA_CHANGE_HANDLE:
            return {
                ...state,
                dataChanged: false,
                dataDeleted: false,
                error: null
            }
        case IDEA_REQUEST:
            return {
                ...state,
                isRequesting: true
            };
        default:
            return state;
    }
}

export default ideaReducer;