import {
    GET_NUTRITION_SUCCESS,
    GET_NUTRITION_ERROR,
    CREATE_NUTRITION_SUCCESS,
    CREATE_NUTRITION_ERROR,
    NUTRITION_CHANGE_HANDLE,
    NUTRITION_REQUEST,
    UPDATE_NUTRITION_SUCCESS,
    UPDATE_NUTRITION_ERROR,
    DELETE_NUTRITION_SUCCESS,
    DELETE_NUTRITION_ERROR
} from '../constants';


const initialState = {
    nutrition: [],
    isRequesting: false,
    dataChanged: false,
    dataDeleted: false,
    error: null
};

const nutritionReducer = (state= initialState, { type, payload }) => {
    switch (type) {
        case GET_NUTRITION_SUCCESS:
            return {
                ...state,
                nutrition: payload.nutrition
            };
        case GET_NUTRITION_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case CREATE_NUTRITION_SUCCESS:
            return {
                ...state,
                nutrition: [...state.nutrition, payload.nutrition],
                dataChanged: true
            };
        case CREATE_NUTRITION_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case UPDATE_NUTRITION_SUCCESS:
            const {id, name, description, recipe, ingredients} = payload.nutrition;

            const updatedNutrition = state.nutrition.map(nutritionElement => {
                if (nutritionElement.id === id) {
                    nutritionElement.name = name;
                    nutritionElement.description = description;
                    nutritionElement.recipe = recipe;
                    nutritionElement.ingredients = ingredients;
                }
                return nutritionElement;
            });

            return {
                ...state,
                nutrition: updatedNutrition,
                dataChanged: true
            };
        case UPDATE_NUTRITION_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case DELETE_NUTRITION_SUCCESS:
            const nutritionWithoutDeleted = state.nutrition.filter(nutritionElement =>
                nutritionElement.id !== payload.id
            );
            return {
                ...state,
                nutrition: nutritionWithoutDeleted,
                dataDeleted: true
            };
        case DELETE_NUTRITION_ERROR:
            return {
                ...state,
                error: payload.error
            };
        case NUTRITION_CHANGE_HANDLE:
            return {
                ...state,
                dataChanged: false,
                dataDeleted: false,
                error: null
            };
        case NUTRITION_REQUEST:
            return {
                ...state,
                isRequesting: true
            };
        default:
            return state;
    }
}

export default nutritionReducer;